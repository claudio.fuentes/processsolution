//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Conector
{
    using System;
    using System.Collections.Generic;
    
    public partial class ESTADO_TAREA
    {
        public ESTADO_TAREA()
        {
            this.HISTORICO_TAREA = new HashSet<HISTORICO_TAREA>();
            this.TAREA = new HashSet<TAREA>();
        }
    
        public decimal ID_ESTADOTAREA { get; set; }
        public string DESCRIPCION { get; set; }
    
        public virtual ICollection<HISTORICO_TAREA> HISTORICO_TAREA { get; set; }
        public virtual ICollection<TAREA> TAREA { get; set; }
    }
}
