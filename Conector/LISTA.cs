//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Conector
{
    using System;
    using System.Collections.Generic;
    
    public partial class LISTA
    {
        public LISTA()
        {
            this.ACTIVIDAD = new HashSet<ACTIVIDAD>();
        }
    
        public decimal ID_LISTA { get; set; }
        public string DESCRIPCION { get; set; }
        public decimal TAREA_ID { get; set; }
    
        public virtual ICollection<ACTIVIDAD> ACTIVIDAD { get; set; }
        public virtual TAREA TAREA { get; set; }
    }
}
