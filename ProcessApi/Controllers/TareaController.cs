﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProcessApi.Auxiliar;
using ProcessApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ProcessApi.Controllers
{
    public class TareaController : ApiController
    {


        /// <summary>
        /// Lista las tareas asociadas al usuario
        /// </summary>
        /// <param name="usuario">id usuario al que recuperar las tareas</param>
        /// <returns>lista de las tareas</returns>
        [HttpPost]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic Tareas(dynamic usuario)
        {

            string token = usuario.headers.ACCESS_TOKEN_NAME;

            int idUsuario = usuario.data.idUsuario;
            try
            {
                if (Token.ValidarToken(token, idUsuario))
                {
                    return Panacea.Respuesta(0, "", new { lista = Tarea.ListaTarea(idUsuario) });
                }
                else
                {
                    return Panacea.Respuesta(-4, "");
                }
            }
            catch (Exception ex)
            {

                return Panacea.Respuesta(-2, "", new { error = ex.Message });
            }
        }

        /// <summary>
        /// Agrega una nueva tarea
        /// </summary>
        /// <param name="tarea">objecto tarea con la información</param>
        /// <returns>estado de la solicitud</returns>
        [HttpPut]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic NuevaTarea(dynamic tarea)
        {
            try
            {
                string token = tarea.headers.ACCESS_TOKEN_NAME;
                int idUsuario = (int)tarea.headers.idUsuario;
                if (Token.ValidarToken(token, idUsuario))
                {
                    Tarea nuevaTarea = new Tarea()
                    {
                        Nivel = (int)tarea.data.nivel,
                        UsuarioID = (int)tarea.data.usuarioID,
                        ProcesoID = (int)tarea.data.procesoID,
                        EstadoTareaID = (int)tarea.data.estadoTareaID,
                        Descripcion = tarea.data.descripcion,
                        Observaciones = tarea.data.observaciones,
                        Inicio = tarea.data.inicio,
                        Termino = tarea.data.termino,
                        InicioRegistrado = tarea.data.inicioRegistrado,
                        TerminoRegistrado = tarea.data.terminoRegistrado,
                        TareaMadreID = tarea.data.tareaMadreID == null ? 0 : (int)tarea.data.tareaMadreID
                    };

                    if (Tarea.AgregarTarea(nuevaTarea))
                    {
                        return Panacea.Respuesta(1, "éxito");
                    }
                    return Panacea.Respuesta(-1, "No fue posible agregar la Tarea");
                }
                else
                {
                    return Panacea.Respuesta(-4);
                }
            }
            catch (Exception ex)
            {
                //Response.StatusCode = 500;
                return Panacea.Respuesta(-2, "", new { error = ex.Message });
            }
        }

        /// <summary>
        /// Permite editar una tarea
        /// </summary>
        /// <param name="tarea"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Tarea/EditarTarea")]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic EditaTarea(dynamic data)
        {
            try
            {
                string token = data.headers.ACCESS_TOKEN_NAME;
                int idUsuario = (int)data.headers.idUsuario;
                if (Token.ValidarToken(token, idUsuario))
                {
                    Tarea nuevaTarea;
                    Tarea.TryParse(data.data.tarea, out nuevaTarea);
                    if (Tarea.EditarTarea(nuevaTarea))
                    {
                        return Panacea.Respuesta(1, "Tarea actualizada");
                    }
                    else
                    {
                        return Panacea.Respuesta(-1, "no fue posible actualizar la tarea");
                    }
                }
                else
                {
                    return Panacea.Respuesta(-4);
                }

            }
            catch (Exception ex)
            {
                return Panacea.Respuesta(-2, "Ha ocurrido una excepción en la ejecución", new { error = ex.Message });
            }
        }

        [HttpPost]
        [Route("api/Tarea/eliminar")]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic EliminarTarea(dynamic data)
        {
            try
            {
                string token = data.headers.ACCESS_TOKEN_NAME;
                int idUsuario = (int)data.headers.idUsuario;
                if (Token.ValidarToken(token,idUsuario))
                {
                    int idTarea = data.data.idTarea;
                    if (Tarea.EliminarTarea(idTarea))
                    {
                        return Panacea.Respuesta(1, "Tarea eliminada");
                    }
                    else
                    {
                        return Panacea.Respuesta(-1, "no ha sido posible eliminar la tarea");
                    }
                }
                else
                {
                    return Panacea.Respuesta(-4);
                }

            }
            catch (Exception ex)
            {
                return Panacea.Respuesta(-2, "", new { error = ex.Message });
            }
        }

        [HttpPost]
        [Route("api/Tarea/actualizarEstado")]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic ActualizarTareaEstado(dynamic data)
        {
            try
            {
                string token = data.headers.ACCESS_TOKEN_NAME;
                int idUsuario = (int)data.headers.idUsuario;
                if (Token.ValidarToken(token, idUsuario))
                {
                    int idTarea = data.data.idTarea,idEstado = data.data.idEstado;
                    
                    if (Tarea.ActualizarEstado(idTarea,idEstado,idUsuario))
                    {
                        return Panacea.Respuesta(1, "Estado actualizado");
                    }
                    else
                    {
                        return Panacea.Respuesta(-1, "no ha sido posible actualizar el estado");
                    }
                }
                else
                {
                    return Panacea.Respuesta(-4);
                }

            }
            catch (Exception ex)
            {
                return Panacea.Respuesta(-2, "", new { error = ex.Message });
            }
        }
    }
}
