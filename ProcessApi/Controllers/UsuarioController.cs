﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ProcessApi.Models;
using ProcessApi.Auxiliar;

namespace ProcessApi.Controllers
{
    //[EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    
    public class UsuarioController : ApiController
    {
        /// <summary>
        /// Valida las credenciales de usuario
        /// </summary>
        /// <param name="usuario">objeto que contiene: nombre de usuario (usuario) contraseña de usuario (contrasenna)</param>
        /// <returns>estado de la solicitud</returns>
        [HttpPost]
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        public dynamic Validar(dynamic usuario) {
            dynamic respuesta;
            try
            {
                string _usuario = usuario.usuario;
                string _contrasenna = usuario.contrasenna;
                _contrasenna = Panacea.Md5(_contrasenna);
                Usuario usuarioValido = Usuario.Validar(_usuario, _contrasenna);

                if (usuarioValido != null)
                {
                    string token = Panacea.GenerateTokenJwt(usuarioValido.IdUsuario.ToString());
                    Token.AgregarToken(new Token(token, (int)usuarioValido.IdUsuario));
                    Sesion sesion = new Sesion(1, token);
                    object data =
                        new
                        {
                            nombreUsuario = usuarioValido.NombreUsuario,
                            idUsuario = usuarioValido.IdUsuario,
                            idPerfil = usuarioValido.PerfilID,
                            token
                        };
                    respuesta = Panacea.Respuesta(0,"",data);
                }
                else
                {
                    respuesta = Panacea.Respuesta(-1, "Usuario o contraseña inválidos");
                }
            }
            catch (Exception ex)
            {
                respuesta = Panacea.Respuesta(-2, "",new { error = ex.Message });
            }

            return respuesta;
        }
    }
}
