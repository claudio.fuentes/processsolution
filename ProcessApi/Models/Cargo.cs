﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class Cargo
    {
        #region PROPIEDADES
        int? idCargo;
        string descripcion;
        #endregion


        #region CONSTRUCTORES
        public Cargo(int? idCargo, string descripcion)
        {
            IdCargo = idCargo;
            Descripcion = descripcion;
        }

        public Cargo(CARGO _cargo)
        {
            IdCargo = (int)_cargo.ID_CARGO;
            Descripcion = _cargo.DESCRIPCION;
        }
        #endregion


        #region ACCESADORES Y MUTADORES
        public int? IdCargo { get => idCargo; set => idCargo = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        #endregion


        #region MÉTODOS

        #endregion


        #region MÉTODOS ESTÁTICOS
        public static List<Cargo> listaCargos()
        {
            try
            {
                List<Cargo> listaRetorno = new List<Cargo>();
                foreach (var item in ConectorDB.Conexion.CARGO)
                {
                    listaRetorno.Add(new Cargo(item));
                }

                return listaRetorno;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static Cargo CargoByID(int id)
        {
            try
            {
                CARGO _cargo = ConectorDB.Conexion.CARGO.First(crg => crg.ID_CARGO == id);

                if (_cargo == null)
                {
                    return null;
                }
                return new Cargo(_cargo);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool AgregarCargo(Cargo _cargo)
        {
            try
            {
                CARGO nuevoCargo = new CARGO();
                int idCargo;

                try
                {
                    idCargo = (int)ConectorDB.Conexion.CARGO.Max(cg => cg.ID_CARGO);
                }
                catch (Exception)
                {
                    idCargo = 0;
                }

                nuevoCargo.ID_CARGO = (decimal)idCargo + 1;
                nuevoCargo.DESCRIPCION = _cargo.Descripcion;

                ConectorDB.Conexion.CARGO.Add(nuevoCargo);
                ConectorDB.Conexion.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EditarCargo(Cargo _cargo)
        {
            try
            {
                CARGO selected = ConectorDB.Conexion.CARGO.First(co => co.ID_CARGO == _cargo.IdCargo);

                selected.ID_CARGO = (int)_cargo.idCargo;
                selected.DESCRIPCION = _cargo.Descripcion;

                ConectorDB.Conexion.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EliminarCArgo(int idCargo)
        {
            try
            {
                ConectorDB.Conexion.CARGO.Remove(ConectorDB.Conexion.CARGO.First(co => co.ID_CARGO == idCargo));
                ConectorDB.Conexion.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int? UsuariosASociados(int _idCargo)
        {
            try
            {
                return ConectorDB.Conexion.CARGO.First(cgo => cgo.ID_CARGO == _idCargo).USUARIO.Count;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

    }
}