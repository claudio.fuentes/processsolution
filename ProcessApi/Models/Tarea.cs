﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessApi.Auxiliar;
using Conector;

namespace ProcessApi.Models
{
    public class Tarea
    {
        #region PROPIEDADES
        int? idTarea, tareaMadreID;
        int nivel, usuarioID, procesoID, estadoTareaID;
        DateTime? inicio, termino, inicioRegistrado, terminoRegistrado;
        string descripcion, observaciones;
        #endregion

        #region CONSTRUCTORES
        public Tarea() { }
        public Tarea(int idTarea, int nivel, int usuarioID, int procesoID, int estadoTareaID, string descripcion, string observaciones = null, DateTime? inicio = null, DateTime? termino = null, DateTime? inicioRegistrado = null, DateTime? terminoRegistrado = null, int? tareaMadreID = null)
        {
            this.idTarea = idTarea;
            this.nivel = nivel;
            this.usuarioID = usuarioID;
            this.procesoID = procesoID;
            this.estadoTareaID = estadoTareaID;
            this.tareaMadreID = (int?)tareaMadreID;
            this.inicio = inicio;
            this.termino = termino;
            this.inicioRegistrado = inicioRegistrado;
            this.terminoRegistrado = terminoRegistrado;
            this.descripcion = descripcion;
            this.observaciones = observaciones;
        }
        public Tarea(TAREA _tarea)
        {
            IdTarea = (int)_tarea.ID_TAREA;
            Nivel = (int)_tarea.NIVEL;
            UsuarioID = (int)_tarea.USUARIO_ID;
            ProcesoID = (int)_tarea.PROCESO_ID;
            EstadoTareaID = (int)_tarea.ESTADOTAREA_ID;
            TareaMadreID = (int)_tarea.TAREAMADRE;
            Descripcion = _tarea.DESCRIPCION;
            Observaciones = _tarea.OBSERVACIONES;
            Inicio = _tarea.INICIO;
            Termino = _tarea.TERMINO;
            InicioRegistrado = _tarea.INICIOREGISTRADO;
            TerminoRegistrado = _tarea.TERMINOREGISTRADO;

        }
        #endregion

        #region GETTER Y SETTER

        public int? IdTarea { get => idTarea; set => idTarea = value; }
        public int Nivel { get => nivel; set => nivel = value; }
        public int UsuarioID { get => usuarioID; set => usuarioID = value; }
        public int ProcesoID { get => procesoID; set => procesoID = value; }
        public int EstadoTareaID { get => estadoTareaID; set => estadoTareaID = value; }
        public int? TareaMadreID { get => tareaMadreID; set => tareaMadreID = value; }
        public DateTime? Inicio { get => inicio; set => inicio = value; }
        public DateTime? Termino { get => termino; set => termino = value; }
        public DateTime? InicioRegistrado { get => inicioRegistrado; set => inicioRegistrado = value; }
        public DateTime? TerminoRegistrado { get => terminoRegistrado; set => terminoRegistrado = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Observaciones { get => observaciones; set => observaciones = value; }
        #endregion

        #region MÉTODOS ESTÁTICOS

        /// <summary>
        /// Lista todas las tareas
        /// </summary>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea()
        {
            try
            {
                int _idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _tareaMadreID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConectorDB.Conexion.TAREA)
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));

                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Lista todas las tareas según usuario
        /// </summary>
        /// <param name="idUsuario">id usuario</param>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea(int idUsuario)
        {
            try
            {
                int? _tareaMadreID;
                int _idTarea,_nivel, _usuarioID, _procesoID, _estadoTareaID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConectorDB.Conexion.TAREA.Where(hw => hw.USUARIO_ID == idUsuario))
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int?)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));

                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Lista todas las tareas según usuario
        /// </summary>
        /// <param name="idUsuario">id usuario</param>
        /// <param name="idProceso">id proceso</param>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea(int idUsuario, int idProceso)
        {
            try
            {
                int _idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _tareaMadreID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConectorDB.Conexion.TAREA.Where(hw => hw.USUARIO_ID == idUsuario && hw.PROCESO_ID == idProceso))
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));
                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// retorna una tarea especifica según su ID
        /// </summary>
        /// <param name="idTarea">id de la tarea a buscar</param>
        /// <param name="tarea">retorno de la tarea encontrada</param>
        /// <returns>estado de la petición</returns>
        public static bool RecuperarTarea(int idTarea, out Tarea tarea)
        {
            try
            {
                TAREA _tarea = ConectorDB.Conexion.TAREA.First(hw => hw.ID_TAREA == idTarea);
                if (_tarea != null)
                {
                    tarea = new Tarea(_tarea);
                    return true;
                }
                else
                {
                    tarea = null;
                    return false;
                }
            }
            catch (Exception ex)
            {
                tarea = null;
                return false;
            }
        }

        /// <summary>
        /// Agrega una nueva tarea al sistema
        /// </summary>
        /// <param name="_tarea">objeto tarea</param>
        /// <returns>estado del inserto</returns>
        public static bool AgregarTarea(Tarea _tarea)
        {
            try
            {
                TAREA nuevaTarea = new TAREA();
                int idTarea;
                try
                {
                    idTarea = ConectorDB.Conexion.TAREA.Count() > 0 ? (int)ConectorDB.Conexion.TAREA.Max(hw => hw.ID_TAREA) + 1 : 1;
                }
                catch (Exception ex)
                {
                    idTarea = 1;
                }


                nuevaTarea.ID_TAREA = (decimal)idTarea;
                nuevaTarea.DESCRIPCION = _tarea.Descripcion;
                nuevaTarea.ESTADOTAREA_ID = _tarea.EstadoTareaID;
                nuevaTarea.OBSERVACIONES = _tarea.Observaciones;
                nuevaTarea.INICIO = _tarea.Inicio;
                nuevaTarea.TERMINO = _tarea.Termino;
                nuevaTarea.NIVEL = _tarea.Nivel;
                nuevaTarea.USUARIO_ID = _tarea.UsuarioID;
                nuevaTarea.PROCESO_ID = _tarea.ProcesoID;
                nuevaTarea.INICIOREGISTRADO = _tarea.InicioRegistrado;
                nuevaTarea.TERMINOREGISTRADO = _tarea.TerminoRegistrado;
                if (_tarea.TareaMadreID != null && _tarea.TareaMadreID != 0)
                {
                    nuevaTarea.TAREAMADRE = (decimal)_tarea.TareaMadreID;
                }



                ConectorDB.Conexion.TAREA.Add(nuevaTarea);
                ConectorDB.Conexion.SaveChanges();
                HistoricoTarea.AgregarHistoricoTarea(new HistoricoTarea(_tarea.EstadoTareaID, _tarea.UsuarioID, idTarea), true);
                return ConectorDB.Conexion.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Permite editar una tarea identificándolo por su ID
        /// </summary>
        /// <param name="tarea">tarea a modificar con sus datos modificados</param>
        /// <returns>estado de la tarea</returns>
        public static bool EditarTarea(Tarea tarea)
        {
            try
            {
                TAREA tareaSelected = ConectorDB.Conexion.TAREA.First(hw => hw.ID_TAREA == tarea.idTarea);

                tareaSelected.ID_TAREA = (int)tarea.IdTarea;
                tareaSelected.DESCRIPCION = tarea.Descripcion;
                tareaSelected.ESTADOTAREA_ID = tarea.EstadoTareaID;
                tareaSelected.OBSERVACIONES = tarea.Observaciones;
                tareaSelected.INICIO = tarea.Inicio;
                tareaSelected.TERMINO = tarea.Termino;
                tareaSelected.NIVEL = tarea.Nivel;
                tareaSelected.USUARIO_ID = tarea.UsuarioID;
                tareaSelected.PROCESO_ID = tarea.ProcesoID;
                tareaSelected.INICIOREGISTRADO = tarea.InicioRegistrado;
                tareaSelected.TERMINOREGISTRADO = tarea.TerminoRegistrado;
                tareaSelected.TAREAMADRE = (decimal)tarea.TareaMadreID;




                ConectorDB.Conexion.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ActualizarEstado(int idTarea,int idEstado,int idUsuario)
        {
            try
            {
                TAREA tareaSelected = ConectorDB.Conexion.TAREA.First(hw => hw.ID_TAREA == idTarea);

                if (tareaSelected.ESTADOTAREA_ID == idEstado)
                {
                    return false;
                }
                
                tareaSelected.ESTADOTAREA_ID = idEstado;
                ConectorDB.Conexion.SaveChanges();
                HistoricoTarea.AgregarHistoricoTarea(new HistoricoTarea(idEstado, idUsuario, idTarea),true);
                return ConectorDB.Conexion.SaveChanges()>0;

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// convierte en Tarea un dato dinámico con iguales parámetros.
        /// </summary>
        /// <param name="data">data a convertir</param>
        /// <param name="tarea">tarea de respuesta</param>
        public static void TryParse(dynamic data, out Tarea tarea)
        {
            try
            {
                tarea = new Tarea()
                {
                    idTarea = data.IdTarea,
                    nivel = data.Nivel,
                    usuarioID = data.UsuarioID,
                    procesoID = data.ProcesoID,
                    estadoTareaID = data.EstadoTareaID,
                    tareaMadreID = data.TareaMadreID,
                    descripcion = data.Descripcion,
                    observaciones = data.Observaciones,
                    inicio = data.Inicio,
                    termino = data.Termino,
                    inicioRegistrado = data.InicioRegistrado,
                    terminoRegistrado = data.TerminoRegistrado
                };
            }
            catch (Exception ex)
            {
                tarea = null;
            }
        }

        public static bool EliminarTarea(int idTarea)
        {
            try
            {
                ConectorDB.Conexion.TAREA.Remove(ConectorDB.Conexion.TAREA.First(tr => tr.ID_TAREA == idTarea));
                ConectorDB.Conexion.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}