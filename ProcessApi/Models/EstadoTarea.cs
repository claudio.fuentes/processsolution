﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class EstadoTarea
    {
        int idEstadoTarea;
        string descripcion;

        public EstadoTarea(int idEstadoTarea, string descripcion)
        {
            this.idEstadoTarea = idEstadoTarea;
            this.descripcion = descripcion;
        }

        public EstadoTarea(ESTADO_TAREA _estadoTarea)
        {
            IdEstadoTarea = (int)_estadoTarea.ID_ESTADOTAREA;
            Descripcion = _estadoTarea.DESCRIPCION;
        }

        public int IdEstadoTarea { get => idEstadoTarea; set => idEstadoTarea = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }


        public static List<EstadoTarea> ListaEstadoTarea()
        {
            try
            {
                List<EstadoTarea> listaRetorno = new List<EstadoTarea>();
                foreach (var item in ConectorDB.Conexion.ESTADO_TAREA)
                {
                    listaRetorno.Add(new EstadoTarea(item));
                }
                return listaRetorno;
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static EstadoTarea EstadoTareaByID(int id)
        {
            try
            {
                ESTADO_TAREA _estadoTarea = ConectorDB.Conexion.ESTADO_TAREA.First(est => est.ID_ESTADOTAREA == id);

                if (_estadoTarea == null)
                {
                    return null;
                }
                return new EstadoTarea(_estadoTarea);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}