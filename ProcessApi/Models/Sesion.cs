﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProcessApi.Models
{
    public class Sesion
    {
        int idUsuario;
        string nombreUsuario;
        string token;

        public Sesion(int idUsuario, string nombreUsuario, string token = null)
        {
            this.idUsuario = idUsuario;
            this.nombreUsuario = nombreUsuario;
            this.token = token;
        }

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Token { get => token; set => token = value; }
    }
}