﻿using Conector;
using ProcessApi.Auxiliar;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProcessApi.Models
{
    public class HistoricoTarea
    {
        
        public HistoricoTarea(int estadoTareaId, int usuarioId, int tareaId)
        {
            this.Fechacambio = DateTime.Now;
            this.EstadoTareaId = estadoTareaId;
            this.UsuarioId = usuarioId;
            this.TareaId = tareaId;
        }
        public HistoricoTarea(int idHistoricoTarea, DateTime fechacambio, int estadoTareaId, int usuarioId, int tareaId)
        {
            this.IdHistoricoTarea = idHistoricoTarea;
            this.Fechacambio = fechacambio;
            this.EstadoTareaId = estadoTareaId;
            this.UsuarioId = usuarioId;
            this.TareaId = tareaId;
        }

        public HistoricoTarea(HISTORICO_TAREA _historicoTarea)
        {
            IdHistoricoTarea = (int)_historicoTarea.ID_HISTORICOTAREA;
            Fechacambio = _historicoTarea.FECHACAMBIO;
            EstadoTareaId = (int)_historicoTarea.ESTADOTAREA_ID;
            UsuarioId = (int)_historicoTarea.USUARIO_ID;
            TareaId = (int)_historicoTarea.TAREA_ID;
        }

        public HistoricoTarea()
        {
        }

        public int IdHistoricoTarea { get; set; }
        public DateTime Fechacambio { get; set; }
        public int EstadoTareaId { get; set; }
        public int UsuarioId { get; set; }
        public int? TareaId { get; set; }


        public static List<HistoricoTarea> ListaHistoricoTarea(bool formatoDia = false)
        {
            try
            {
                List<HistoricoTarea> listaRetorno = new List<HistoricoTarea>();
                foreach (HISTORICO_TAREA item in ConectorDB.Conexion.HISTORICO_TAREA)
                {
                    listaRetorno.Add(new HistoricoTarea(item));
                }
                if (formatoDia)
                {
                    foreach (HistoricoTarea historico in listaRetorno)
                    {
                        historico.Fechacambio = historico.Fechacambio.Date;
                    }
                }
                return listaRetorno;
            }

            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static List<HistoricoTarea> ListaHistoricoTarea(int idEmpresa,bool formatoDia = false)
        {
            try
            {
                List<HistoricoTarea> listaRetorno = new List<HistoricoTarea>();

                var listaHt = ConectorDB.Conexion.HISTORICO_TAREA.SqlQuery($"SELECT ht.* FROM historico_tarea ht INNER JOIN tarea hw ON hw.ID_tarea = ht.tarea_id INNER JOIN proceso prs ON prs.id_proceso = hw.proceso_id WHERE prs.empresa_id = {idEmpresa}");

                foreach (var item in listaHt)
                {
                    listaRetorno.Add(new HistoricoTarea(item));
                }
                if (formatoDia)
                {
                    foreach (HistoricoTarea historico in listaRetorno)
                    {
                        historico.Fechacambio = historico.Fechacambio.Date;
                    }
                }
                return listaRetorno;
            }

            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static List<HistoricoTarea> ListaHistoricoTareaByProceso(int idProceso, bool formatoDia = false)
        {
            try
            {
                List<HistoricoTarea> listaRetorno = new List<HistoricoTarea>();

                var listaHt = ConectorDB.Conexion.HISTORICO_TAREA.SqlQuery($"SELECT ht.* FROM historico_tarea ht INNER JOIN tarea hw ON hw.ID_tarea = ht.tarea_id WHERE hw.proceso_id = {idProceso}");

                foreach (var item in listaHt)
                {
                    listaRetorno.Add(new HistoricoTarea(item));
                }
                if (formatoDia)
                {
                    foreach (HistoricoTarea historico in listaRetorno)
                    {
                        historico.Fechacambio = historico.Fechacambio.Date;
                    }
                }
                return listaRetorno;
            }

            catch (Exception ex)
            {
                return null;
                throw;
            }
        }

        public static HistoricoTarea historicoTareaById(int id)
        {
            try
            {
                HISTORICO_TAREA _historicoTarea = ConectorDB.Conexion.HISTORICO_TAREA.First(htc => htc.ID_HISTORICOTAREA == id);
                if (_historicoTarea == null)
                {
                    return null;
                }
                return new HistoricoTarea(_historicoTarea);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool AgregarHistoricoTarea(HistoricoTarea _historicoTarea,bool sinSave = false)
        {
            try
            {
                HISTORICO_TAREA nuevoHistorico = new HISTORICO_TAREA();
                int idHistoricoTarea;
                try
                {
                    idHistoricoTarea = ConectorDB.Conexion.HISTORICO_TAREA.Count() > 0?(int)ConectorDB.Conexion.HISTORICO_TAREA.Max(ht => ht.ID_HISTORICOTAREA):0;
                }
                catch (Exception)
                {
                    idHistoricoTarea = 0;
                }

                nuevoHistorico.ID_HISTORICOTAREA = (decimal)idHistoricoTarea + 1;
                nuevoHistorico.FECHACAMBIO = _historicoTarea.Fechacambio;
                nuevoHistorico.ESTADOTAREA_ID = _historicoTarea.EstadoTareaId;
                nuevoHistorico.USUARIO_ID = _historicoTarea.UsuarioId;
                nuevoHistorico.TAREA_ID = (int)_historicoTarea.TareaId;


                ConectorDB.Conexion.HISTORICO_TAREA.Add(nuevoHistorico);
                if (sinSave)
                    return true;

                return ConectorDB.Conexion.SaveChanges() > 0;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static bool EliminarHistoricoTarea(int idHistoricoTarea)
        {
            try
            {
                ConectorDB.Conexion.HISTORICO_TAREA.Remove(ConectorDB.Conexion.HISTORICO_TAREA.First(ht => ht.ID_HISTORICOTAREA == idHistoricoTarea));
                ConectorDB.Conexion.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int EliminarListadoHistoricoTarea(int[] listaIdHistoricoTarea)
        {
            try
            {
                foreach (int item in listaIdHistoricoTarea)
                {
                    ConectorDB.Conexion.HISTORICO_TAREA.Remove(ConectorDB.Conexion.HISTORICO_TAREA.First(ht => ht.ID_HISTORICOTAREA == item));
                }

                return ConectorDB.Conexion.SaveChanges();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }

}
