﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class Usuario
    {
        int? idUsuario;
        string nombreUsuario;
        string nombre;
        string apellido;
        string correo;
        int perfilID, cargoID, empresaID;

        #region CONSTRUCTORES
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public Usuario() { }

        /// <summary>
        /// Constructor estándar
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <param name="nombreUsuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellido"></param>
        /// <param name="correo"></param>
        /// <param name="perfilID"></param>
        /// <param name="cargoID"></param>
        /// <param name="empresaID"></param>
        public Usuario(int idUsuario, string nombreUsuario, string nombre, string apellido, string correo, int perfilID, int cargoID, int empresaID)
        {
            IdUsuario = idUsuario;
            NombreUsuario = nombreUsuario;
            Nombre = nombre;
            Apellido = apellido;
            Correo = correo;
            PerfilID = perfilID;
            CargoID = cargoID;
            EmpresaID = empresaID;
        }

        /// <summary>
        /// Constructor estándar
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="nombre"></param>
        /// <param name="apellido"></param>
        /// <param name="correo"></param>
        /// <param name="perfilID"></param>
        /// <param name="cargoID"></param>
        /// <param name="empresaID"></param>
        public Usuario(string nombreUsuario, string nombre, string apellido, string correo, int perfilID, int cargoID, int empresaID)
        {
            IdUsuario = null;
            NombreUsuario = nombreUsuario;
            Nombre = nombre;
            Apellido = apellido;
            Correo = correo;
            PerfilID = perfilID;
            CargoID = cargoID;
            EmpresaID = empresaID;
        }

        /// <summary>
        /// crea un usuario a partir de una clase en entity framework
        /// </summary>
        /// <param name="_usuario">usuario raíz</param>
        public Usuario(USUARIO _usuario)
        {
            IdUsuario = (int)_usuario.ID_USUARIO;
            NombreUsuario = _usuario.NOMBREUSUARIO;
            Nombre = _usuario.NOMBRE;
            Apellido = _usuario.APELLIDOS;
            Correo = _usuario.CORREO;
            PerfilID = (int)_usuario.PERFIL_ID;
            CargoID = (int)_usuario.CARGO_ID;
            EmpresaID = (int)_usuario.EMPRESA_ID;
        }

        /// <summary>
        /// genera un nuevo usuario para la integración de la base de datos a partir del usuario instanciado
        /// </summary>
        /// <returns>Usuario en condiciones de ser almacenado en la base de datos</returns>
        public USUARIO usuarioDB(string contrasenna)
        {
            try
            {
                decimal _idUsuario = IdUsuario == null ? ConectorDB.Conexion.USUARIO.Max(usu => usu.ID_USUARIO) + 1 : (decimal)IdUsuario;
                return new USUARIO()
                {
                    ID_USUARIO = _idUsuario,
                    NOMBREUSUARIO = this.NombreUsuario,
                    CORREO=this.Correo,
                    NOMBRE = this.Nombre,
                    APELLIDOS = this.Apellido,
                    CONTRASENNA = contrasenna,
                    CARGO_ID = this.CargoID,
                    EMPRESA_ID = this.EmpresaID,
                    PERFIL_ID = this.PerfilID
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion


        #region ACCESADORES Y MUTADORES
        public int? IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Correo { get => correo; set => correo = value; }
        public int PerfilID { get => perfilID; set => perfilID = value; }
        public int CargoID { get => cargoID; set => cargoID = value; }
        public int EmpresaID { get => empresaID; set => empresaID = value; }
        #endregion


        #region MÉTODOS
        /// <summary>
        /// Lista las tareas asociadas al usuario
        /// </summary>
        /// <returns></returns>
        public List<Tarea> listaTareasAsociadas()
        {
            try
            {
                List<Tarea> listaTareas = new List<Tarea>();

                foreach (TAREA item in ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == (decimal)IdUsuario).TAREA)
                {
                    listaTareas.Add(new Tarea(item));
                }

                return listaTareas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<HistoricoTarea> listaHistoricoTareasAsociadas()
        {
            try
            {
                List<HistoricoTarea> listaHT = new List<HistoricoTarea>();

                foreach (HISTORICO_TAREA item in ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == (decimal)IdUsuario).HISTORICO_TAREA)
                {
                    listaHT.Add(new HistoricoTarea(item));
                }
                return listaHT;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion


        #region MÉTODOS ESTÁTICOS
        /// <summary>
        /// Valida las credenciales de usuario en la base de datos
        /// </summary>
        /// <param name="usuario">nombre de usuario</param>
        /// <param name="contrasenna">contraseña ya hasheada</param>
        /// <returns></returns>
        public static Usuario Validar(string usuario, string contrasenna)
        {
            try
            {
                USUARIO _usuario = ConectorDB.Conexion.USUARIO.First(usu => usu.CONTRASENNA == contrasenna && usu.NOMBREUSUARIO == usuario);

                if (_usuario != null)
                {
                    return new Usuario(
                        (int)_usuario.ID_USUARIO,
                        _usuario.NOMBREUSUARIO,
                        _usuario.NOMBRE,
                        _usuario.APELLIDOS,
                        _usuario.CORREO,
                        (int)_usuario.PERFIL_ID,
                        (int)_usuario.CARGO_ID,
                        (int)_usuario.EMPRESA_ID);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {

                return null;
            }
        }

        /// <summary>
        /// lista todos los usuarios
        /// </summary>
        /// <returns>lista de usuarios</returns>
        public static List<Usuario> ListaUsuarios()
        {
            try
            {
                List<Usuario> lista = new List<Usuario>();
                foreach (var item in ConectorDB.Conexion.USUARIO)
                {
                    lista.Add(new Usuario(item));
                }

                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// lista de usuarios customizada
        /// </summary>
        /// <returns>lista de usuarios</returns>
        public static List<dynamic> ListaUsuariosCustom()
        {
            try
            {
                List<dynamic> listaRetorno = new List<dynamic>();
                foreach (var item in ConectorDB.Conexion.USUARIO)
                {
                    listaRetorno.Add(new
                    {
                        idUsuario = item.ID_USUARIO,
                        nombreUsuario = item.NOMBREUSUARIO,
                        nombre = item.NOMBRE,
                        apellido = item.APELLIDOS,
                        correo = item.CORREO,
                        perfilID = item.PERFIL_ID,
                        perfilNombre = item.PERFIL.DESCRIPCION,
                        cargoID = item.CARGO_ID,
                        cargoNombre = item.CARGO.DESCRIPCION,
                        empresaID = item.EMPRESA_ID,
                        empresaNombre = item.EMPRESA.RAZONSOCIAL
                    });
                }

                return listaRetorno;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        /// <summary>
        /// Agrega un nuevo usuario a la base de datos
        /// </summary>
        /// <param name="usuario">objeto usuario con la información parcial</param>
        /// <param name="contrasenna">contraseña (no incluída en el objeto usuario)</param>
        /// <returns>estado de la petición</returns>
        public static bool AgregarUsuario(Usuario usuario, string contrasenna)
        {
            try
            {
                ConectorDB.Conexion.USUARIO.Add(usuario.usuarioDB(contrasenna));
                return ConectorDB.Conexion.SaveChanges() == 1;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Permite editar un usuario de la base de datos
        /// </summary>
        /// <param name="usuario">objeto usuario con la información parcial</param>
        /// <param name="contrasenna">contraseña (no incluída en el objeto usuario)</param>
        /// <returns>estado de la petición</returns>
        public static bool EdicionUsuario(Usuario usuario, string contrasenna)
        {
            try
            {
                USUARIO _usuario = ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == usuario.IdUsuario);
                _usuario.NOMBRE = usuario.Nombre;
                _usuario.NOMBREUSUARIO = usuario.NombreUsuario;
                _usuario.NOMBRE = usuario.Nombre;
                _usuario.APELLIDOS = usuario.Apellido;
                _usuario.CONTRASENNA = contrasenna != null ? contrasenna : _usuario.CONTRASENNA;
                _usuario.CARGO_ID = usuario.CargoID;
                _usuario.EMPRESA_ID = usuario.EmpresaID;
                _usuario.PERFIL_ID = usuario.PerfilID;

                return ConectorDB.Conexion.SaveChanges() == 1;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EliminarUsuarioHT(int _idUsuario, out int eliminados)
        {
            try
            {
                if (ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == _idUsuario).TAREA.Count > 0)
                {
                    eliminados = 0;
                    return false;
                }

                List<int> listaHT = new List<int>();
                foreach (HISTORICO_TAREA item in ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == (decimal)_idUsuario).HISTORICO_TAREA)
                {
                    listaHT.Add((int)item.ID_HISTORICOTAREA);
                }
                eliminados = HistoricoTarea.EliminarListadoHistoricoTarea(listaHT.ToArray());
                
                ConectorDB.Conexion.USUARIO.Remove(ConectorDB.Conexion.USUARIO.First(usu => usu.ID_USUARIO == _idUsuario));

                return ConectorDB.Conexion.SaveChanges() > 0;

            }
            catch (Exception ex)
            {
                eliminados = 0;
                return false;
            }
        }
        #endregion


    }
}