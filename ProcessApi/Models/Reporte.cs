﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class Reporte
    {
        public partial class EstadosEnElTiempo
        {
            public EstadosEnElTiempo() { }
            public int Cantidad { get; set; }
            public DateTime Fecha { get; set; }
            public int Estado { get; set; }
        }
        public enum Periodo {semana,mes,meses}
        public static List<EstadosEnElTiempo> EstadosEnLosTiempos(Periodo periodo = Periodo.semana)
        {
            DateTime finIntervalo = DateTime.Now,comienzoIntervalo= new DateTime();
            switch (periodo)
            {
                case Periodo.semana:
                    comienzoIntervalo = finIntervalo.AddDays(-7);
                    break;
                case Periodo.mes:
                    comienzoIntervalo = finIntervalo.AddDays(-30);
                    break;
                case Periodo.meses:
                    comienzoIntervalo = finIntervalo.AddDays(-60);
                    break;
            }


            try
            {
                var query = HistoricoTarea.ListaHistoricoTarea(true)
                    .GroupBy(ht => new { ht.Fechacambio, ht.EstadoTareaId })
                    .Select(ht => new { ht.Key.EstadoTareaId, ht.Key.Fechacambio, cantidad = ht.Count() });

                List<EstadosEnElTiempo> historicos = new List<EstadosEnElTiempo>();

                do
                {
                    if (query.Any(x => x.Fechacambio == comienzoIntervalo.Date))
                    {
                        var subquery = query.Where(x => x.Fechacambio == comienzoIntervalo.Date);
                        foreach (var item in subquery)
                        {
                            historicos.Add(new EstadosEnElTiempo()
                            {
                                Fecha = item.Fechacambio,
                                Estado = item.EstadoTareaId,
                                Cantidad = item.cantidad
                            });
                        }
                    }
                    else
                    {
                        historicos.Add(new EstadosEnElTiempo() { 
                        Cantidad=0,
                        Estado = 1,
                        Fecha=comienzoIntervalo.Date
                        });
                        historicos.Add(new EstadosEnElTiempo()
                        {
                            Cantidad = 0,
                            Estado = 2,
                            Fecha = comienzoIntervalo.Date
                        });
                    }
                    comienzoIntervalo = comienzoIntervalo.AddDays(1);
                } while (comienzoIntervalo <= finIntervalo);

                
                return historicos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<EstadosEnElTiempo> EstadosEnLosTiempos(int idEmpresa,Periodo periodo = Periodo.semana)
        {
            DateTime finIntervalo = DateTime.Now, comienzoIntervalo = new DateTime();
            switch (periodo)
            {
                case Periodo.semana:
                    comienzoIntervalo = finIntervalo.AddDays(-7);
                    break;
                case Periodo.mes:
                    comienzoIntervalo = finIntervalo.AddDays(-30);
                    break;
                case Periodo.meses:
                    comienzoIntervalo = finIntervalo.AddDays(-60);
                    break;
            }
            try
            {
                var query = HistoricoTarea.ListaHistoricoTarea(idEmpresa, true)
                    .GroupBy(ht => new { ht.Fechacambio, ht.EstadoTareaId })
                    .Select(ht => new { ht.Key.EstadoTareaId, ht.Key.Fechacambio, cantidad = ht.Count() });

                List<EstadosEnElTiempo> historicos = new List<EstadosEnElTiempo>();

                do
                {
                    if (query.Any(x => x.Fechacambio == comienzoIntervalo.Date))
                    {
                        var subquery = query.Where(x => x.Fechacambio == comienzoIntervalo.Date);
                        foreach (var item in subquery)
                        {
                            historicos.Add(new EstadosEnElTiempo()
                            {
                                Fecha = item.Fechacambio,
                                Estado = item.EstadoTareaId,
                                Cantidad = item.cantidad
                            });
                        }
                    }
                    else
                    {
                        historicos.Add(new EstadosEnElTiempo()
                        {
                            Cantidad = 0,
                            Estado = 1,
                            Fecha = comienzoIntervalo.Date
                        });
                        historicos.Add(new EstadosEnElTiempo()
                        {
                            Cantidad = 0,
                            Estado = 2,
                            Fecha = comienzoIntervalo.Date
                        });
                    }
                    comienzoIntervalo = comienzoIntervalo.AddDays(1);
                } while (comienzoIntervalo <= finIntervalo);

               
                return historicos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<EstadosEnElTiempo> EstadosEnLosTiemposByProceso(int idProceso, Periodo periodo)
        {
            DateTime finIntervalo = DateTime.Now, comienzoIntervalo = new DateTime();
            switch (periodo)
            {
                case Periodo.semana:
                    comienzoIntervalo = finIntervalo.AddDays(-7);
                    break;
                case Periodo.mes:
                    comienzoIntervalo = finIntervalo.AddDays(-30);
                    break;
                case Periodo.meses:
                    comienzoIntervalo = finIntervalo.AddDays(-60);
                    break;
            }
            try
            {
                var query = HistoricoTarea.ListaHistoricoTareaByProceso(idProceso, true)
                    .GroupBy(ht => new { ht.Fechacambio, ht.EstadoTareaId })
                    .Select(ht => new { ht.Key.EstadoTareaId, ht.Key.Fechacambio, cantidad = ht.Count() });

                List<EstadosEnElTiempo> historicos = new List<EstadosEnElTiempo>();

                do
                {
                    if (query.Any(x => x.Fechacambio == comienzoIntervalo.Date))
                    {
                        var subquery = query.Where(x => x.Fechacambio == comienzoIntervalo.Date);
                        foreach (var item in subquery)
                        {
                            historicos.Add(new EstadosEnElTiempo()
                            {
                                Fecha = item.Fechacambio,
                                Estado = item.EstadoTareaId,
                                Cantidad = item.cantidad
                            });
                        }
                    }
                    else
                    {
                        historicos.Add(new EstadosEnElTiempo()
                        {
                            Cantidad = 0,
                            Estado = 1,
                            Fecha = comienzoIntervalo.Date
                        });
                        historicos.Add(new EstadosEnElTiempo()
                        {
                            Cantidad = 0,
                            Estado = 2,
                            Fecha = comienzoIntervalo.Date
                        });
                    }
                    comienzoIntervalo = comienzoIntervalo.AddDays(1);
                } while (comienzoIntervalo <= finIntervalo);

                return historicos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}