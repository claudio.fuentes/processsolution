﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class Perfil
    {
        int idPerfil;
        string descripcion;

        public Perfil(int idPerfil, string descripcion)
        {
            IdPerfil = idPerfil;
            Descripcion = descripcion;
        }

        public Perfil(PERFIL _perfil)
        {
            IdPerfil = (int)_perfil.ID_PERFIL;
            Descripcion = _perfil.DESCRIPCION;
        }

        public int IdPerfil { get => idPerfil; set => idPerfil = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }

        public static Perfil perfilByID(int id) {
            try
            {
                PERFIL _perfil = ConectorDB.Conexion.PERFIL.First(per => per.ID_PERFIL == id);
                if (_perfil == null)
                {
                    return null;
                }
                return new Perfil(_perfil);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<Perfil> listaPerfiles() {
            try
            {
                List<Perfil> listaRetorno = new List<Perfil>();

                foreach (var item in ConectorDB.Conexion.PERFIL)
                {
                    listaRetorno.Add(new Perfil(item));
                }

                return listaRetorno;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}