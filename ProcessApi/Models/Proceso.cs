﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessApi.Auxiliar;
using Conector;
using System.Data.Entity;

namespace ProcessApi.Models
{
    public class Proceso
    {

        int idProceso, idEmpresa;
        string descripcion, detalle;
        bool modelo;
        DateTime inicio;
        DateTime? termino;

        public Proceso(int idProceso, int idEmpresa, string descripcion, string detalle, bool modelo, DateTime inicio, DateTime? termino)
        {
            this.IdProceso = idProceso;
            this.IdEmpresa = idEmpresa;
            this.Descripcion = descripcion;
            this.Detalle = detalle;
            this.Modelo = modelo;
            this.Inicio = inicio;
            this.Termino = termino;
        }


        public int IdProceso { get => idProceso; set => idProceso = value; }
        public int IdEmpresa { get => idEmpresa; set => idEmpresa = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Detalle { get => detalle; set => detalle = value; }
        public bool Modelo { get => modelo; set => modelo = value; }
        public DateTime Inicio { get => inicio; set => inicio = value; }
        public DateTime? Termino { get => termino; set => termino = value; }



        public static List<Proceso> listaProcesoCompleto()
        {
            try
            {
                List<Proceso> listaProcesos = new List<Proceso>();
                foreach (var item in ConectorDB.Conexion.PROCESO)
                {
                    int _idProceso, _idEmpresa;
                    string _descripcion, _detalle;
                    bool _modelo;
                    DateTime _inicio;
                    DateTime? _termino;

                    _idProceso = (int)item.ID_PROCESO;
                    _idEmpresa = (int)item.EMPRESA_ID;
                    _descripcion = item.DESCRIPCION;
                    _detalle = item.DETALLE;
                    _modelo = item.MODELO == "1" ? true : false;
                    _inicio = item.INICIO;
                    _termino = (DateTime?)item.TERMINO;

                    listaProcesos.Add(new Proceso(_idProceso, _idEmpresa, _descripcion, _detalle, _modelo, _inicio, _termino));
                }


                return listaProcesos;
            }
            catch (Exception ex)
            {

                throw;
            }
        }


        public static List<dynamic> listaCustomizada()
        {
            try
            {
                List<dynamic> listaProcesosCustom = new List<dynamic>();

                foreach (var item in ConectorDB.Conexion.PROCESO)
                {
                    if (item.MODELO == "0")
                    {
                        dynamic procesoCustom =
                        new
                        {
                            idProceso = (int)item.ID_PROCESO,
                            empresa = item.EMPRESA.RAZONSOCIAL,
                            proceso = item.DESCRIPCION,
                            inicio = item.INICIO,
                            termino = item.TERMINO,
                            tareas = item.TAREA.Count,
                            tareasFinalizadas = item.TAREA.Where(tar => tar.TERMINOREGISTRADO != null).Count()
                        };
                        listaProcesosCustom.Add(procesoCustom);
                    }
                }
                return listaProcesosCustom;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static bool agregaProceso(Proceso proceso)
        {
            try
            {
                PROCESO _proceso = new PROCESO();
                _proceso.DESCRIPCION = proceso.Descripcion;
                _proceso.DETALLE = proceso.Detalle;
                _proceso.EMPRESA_ID = proceso.IdEmpresa;
                _proceso.ID_PROCESO = proceso.IdProceso = ConectorDB.Conexion.PROCESO.Max(prs => (int)prs.ID_PROCESO) + 1;
                _proceso.INICIO = proceso.Inicio;
                _proceso.MODELO = proceso.Modelo ? "1" : "0";
                _proceso.TERMINO = proceso.Termino;
                ConectorDB.Conexion.PROCESO.Add(_proceso);

                ConectorDB.Conexion.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Proceso buscarPorID(int idProceso)
        {
            try
            {
                DbSet<PROCESO> resultado = ConectorDB.Conexion.PROCESO;
                foreach (var item in resultado)
                {
                    var algo = item.ID_PROCESO;
                }
                PROCESO _proceso = resultado.First(pr => pr.ID_PROCESO == idProceso);

                if (_proceso != null)
                {
                    return
                    new Proceso(
                        idProceso,
                        (int)_proceso.EMPRESA_ID,
                        _proceso.DESCRIPCION,
                        _proceso.DETALLE,
                        _proceso.MODELO == "1" ? true : false,
                        _proceso.INICIO,
                        _proceso.TERMINO
                        );
                }
                else
                {
                    return null;
                }


            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool procesoByModelo(int idProceso)
        {
            try
            {
                Proceso _procesoModelo = buscarPorID(idProceso);
                if (_procesoModelo.Modelo)
                {
                    _procesoModelo.Modelo = false;
                    agregaProceso(_procesoModelo);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


    }

}