﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;
using ProcessApi.Auxiliar;

namespace ProcessApi.Models
{
    public class Empresa
    {
        int? idEmpresa;
        string razonSocial;

        public Empresa(int? idEmpresa, string razonSocial)
        {
            this.IdEmpresa = idEmpresa;
            this.RazonSocial = razonSocial;
        }

        public int? IdEmpresa { get => idEmpresa; set => idEmpresa = value; }
        public string RazonSocial { get => razonSocial; set => razonSocial = value; }

        public Empresa(EMPRESA _empresa) {
            IdEmpresa = (int)_empresa.ID_EMPRESA;
            RazonSocial = _empresa.RAZONSOCIAL;
        }

        public static List<Empresa> listaEmpresas()
        {
            try
            {
                  List<Empresa> listaEmpresas = new List<Empresa>();

                foreach (var item in ConectorDB.Conexion.EMPRESA.Where(i => i.ID_EMPRESA > 0))
                {
                    int _idEmpresa = (int)item.ID_EMPRESA;
                    string _razonSocial = item.RAZONSOCIAL;

                    listaEmpresas.Add(new Empresa(_idEmpresa, _razonSocial));
                }

                return listaEmpresas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Empresa EmpresaByID(int id) {
            try
            {
                EMPRESA _empresa = ConectorDB.Conexion.EMPRESA.First(emp => emp.ID_EMPRESA == id);
                if (_empresa != null)
                {
                    return new Empresa(_empresa);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool AgregarEmpresa(Empresa _empresa)
        {
            try
            {
                
                int idEmpresa;
                try
                {
                    idEmpresa = (int)ConectorDB.Conexion.EMPRESA.Max(em => em.ID_EMPRESA) +1;
                    EMPRESA nuevaEmpresa = new EMPRESA()
                    {
                        ID_EMPRESA = idEmpresa,
                        RAZONSOCIAL = _empresa.RazonSocial
                    };
                    ConectorDB.Conexion.EMPRESA.Add(nuevaEmpresa);
                    return ConectorDB.Conexion.SaveChanges() > 0;
                }
                catch (Exception ex)
                {
                    return false;
                }

                
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EditarEmpresa(Empresa _empresa)
        {
            try
            {
                EMPRESA selected = ConectorDB.Conexion.EMPRESA.First(em => em.ID_EMPRESA == _empresa.IdEmpresa);
                selected.RAZONSOCIAL = _empresa.RazonSocial;
                return ConectorDB.Conexion.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EliminarEmpresa(int id)
        {
            try
            {
                ConectorDB.Conexion.EMPRESA.Remove(ConectorDB.Conexion.EMPRESA.First(tr => tr.ID_EMPRESA == id));
                return ConectorDB.Conexion.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// lista los usuarios y procesos asociados a una empresa.
        /// </summary>
        /// <param name="idEmpresa">id empresa a buscar</param>
        /// <returns>lista de usuarios y procesos asociados, la llave indica a cual pertenece cada dato 
        /// y el valor es el total</returns>
        public static Dictionary<string,int> UsuariosYProcesosASociados(int _idEmpresa)
        {
            try
            {
                Dictionary<string, int> listaAsociados = new Dictionary<string, int>();
                try
                {
                    listaAsociados.Add("usuario", ConectorDB.Conexion.EMPRESA.First(emp => emp.ID_EMPRESA == _idEmpresa).USUARIO.Count);
                    listaAsociados.Add("proceso", ConectorDB.Conexion.EMPRESA.First(emp => emp.ID_EMPRESA == _idEmpresa).PROCESO.Count);
                }
                catch (Exception ex)
                {
                    listaAsociados.Add("usuario", 0);
                    listaAsociados.Add("proceso", 0);
                }
                

                return listaAsociados;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}