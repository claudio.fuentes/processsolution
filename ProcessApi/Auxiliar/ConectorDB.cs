﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;

namespace ProcessApi.Auxiliar
{
    public class ConectorDB
    {
        static EntitiesConexiones conexion;

        public static EntitiesConexiones Conexion { 
            get 
            {
                if (conexion == null)
                {
                    conexion = new EntitiesConexiones();
                }
                return conexion; 
            }  
        }

        ConectorDB() { 
        
        }
    }
}