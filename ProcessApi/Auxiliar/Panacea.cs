﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Web;

namespace ProcessApi.Auxiliar
{
    public static class Panacea
    {
        /// <summary>formato de dato para envío y recepción</summary>
        public static string JSONTYPE = "application/json";
        /// <summary> nombre dado al token de envío</summary>
        public static string ACCESS_TOKEN_NAME = "login_access_token";

        /// <summary>
        /// Hashea valores con MD5
        /// </summary>
        /// <param name="Value">valor a Hashear</param>
        /// <returns>valor Hasheado</returns>
        public static string Md5(string Value)
        {
            MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
            return ret;
        }

        public static string GenerateTokenJwt(string username)
        {
            var jwtTokenString = "";
            try
            {
                // appsetting for Token JWT
                var secretKey = ConfigurationManager.AppSettings["JWT_SECRET_KEY"];
                var audienceToken = ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
                var issuerToken = ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
                var expireTime = ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"];

                SymmetricSecurityKey securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(secretKey));
                SigningCredentials signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

                // create a claimsIdentity
                ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, username) });

                // create token to the user
                var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
                var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                    audience: audienceToken,
                    issuer: issuerToken,
                    subject: claimsIdentity,
                    notBefore: DateTime.UtcNow,
                    expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                    signingCredentials: signingCredentials);

                jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);
            }
            catch (Exception ex)
            {
                return null;
            }
            return jwtTokenString;
        }

        /// <summary>
        /// Formatea mensaje de respuesta
        /// </summary>
        /// <param name="codigo">codigo estado de la respuesta. negativo para errores</param>
        /// <param name="mensaje">mensaje para envío al cliente, en blanco o nulo para llenado según código</param>
        /// <param name="data">información adicional relacionada con la petición</param>
        /// <returns>objeto dinámico estandarizado para respuesta a cliente</returns>
        public static dynamic Respuesta(int codigo,string mensaje = null,dynamic data = null) {

            if (mensaje == null || mensaje == "")
            {
                switch (codigo)
                {
                    case 1:
                    case 0:
                        mensaje = "éxito";
                        break;
                    case -2:
                        mensaje = "Ha ocurrido un error en el servidor";
                        break;
                    case -4:
                        mensaje = "tóken inválido";
                        break;
                    default:
                        mensaje = "";
                        break;
                }
            }

            return new { codigo, mensaje, data };

        }
    }
}