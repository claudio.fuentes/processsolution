﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProcessService.Auxiliar;
using ProcessService.Models;
using Newtonsoft.Json;

namespace ProcessService.Controllers
{
    /// <summary>
    /// Accede a las funciones relacionadas con los procesos
    /// </summary>
    public class ProcesoController : Controller
    {

        #region FUNCIONES BÁSICAS
        /// <summary>
        /// Lista los procesos en la base de datos
        /// </summary>
        /// <returns>lista de procesos</returns>
        public object procesos()
        {
            Response.ContentType = "application/json";
            try
            {
                List<Proceso> procesos = Proceso.listaProcesoCompleto();

                Response.StatusCode = 200;
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(0, "éxito", new { cantidad = procesos.Count, procesos }));
            }
            catch (Exception ex)
            {
                return GestionAuxiliar.FormatoRespuesta(-2, "ha ocurrido un error en la ejecución", ex.Message);
                throw;
            }
        }

        /// <summary>
        /// retorna el proceso buscado por ID
        /// </summary>
        /// <param name="idProceso">ID del proceso a buscar</param>
        /// <returns>proceso encontrado</returns>
        [HttpGet]
        public dynamic proceso(int idProceso) {
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            try
            {
                var respuesta = Proceso.buscarPorID(idProceso);
                Response.StatusCode = respuesta != null ? 200 : 404;
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(0,respuesta!=null?"éxito":"proceso no encontrado",respuesta));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-1, "Ha ocurrido un error en el servidor", new { error = ex.Message }));
            }
        }

        /// <summary>
        /// Agrega un nuevo proceso
        /// </summary>
        /// <param name="Descripcion"></param>
        /// <param name="Detalle"></param>
        /// <param name="IdEmpresa"></param>
        /// <param name="Inicio"></param>
        /// <param name="Modelo"></param>
        /// <param name="Termino"></param>
        /// <returns></returns>
        [HttpPut]
        public object proceso(int IdEmpresa, string Descripcion, string Detalle, bool Modelo, DateTime Inicio, DateTime? Termino)
        {
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            try
            {
                Proceso _proceso = new Proceso(0, IdEmpresa, Descripcion, Detalle, Modelo, Inicio, Termino);
                if (Proceso.agregaProceso(_proceso))
                {
                    Response.StatusCode = 201;
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(1, "proceso creado", _proceso));
                }
                else
                {
                    Response.StatusCode = 500;
                    return GestionAuxiliar.Respuesta(-1, "Ha ocurrido un error en la creación");
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return  (codigo: 1, mensaje: "ha ocurrido un error interno", data: ex.Message);

            }
        }
        #endregion


        #region FUNCIONES CUSTOMIZABLES
        /// <summary>
        /// Lista los procesos que no sean modelos
        /// </summary>
        /// <returns>lista customizada de los procesos</returns>
        public dynamic procesoCustom()
        {
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            Request.ContentType = GestionAuxiliar.JSONTYPE;

            try
            {
                List<dynamic> procesos = Proceso.listaCustomizada();

                Response.StatusCode = 200;
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(0, "éxito", new { cantidad = procesos.Count, procesos }));
            }
            catch (Exception ex)
            {
                return GestionAuxiliar.FormatoRespuesta(-2, "ha ocurrido un error en la ejecución", ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Lista customizada de procesos tipo modelo
        /// </summary>
        /// <returns>Lista customizada</returns>
        [HttpGet]
        public dynamic procesoModelo() {
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            try
            {
                List<dynamic> listaModeloFiltrado = new List<dynamic>();
                List<Proceso> listaModelo = new List<Proceso>();
                listaModelo.AddRange(Proceso.listaProcesoCompleto().Where(lpc => lpc.Modelo));
                

                foreach (var item in listaModelo)
                {
                    dynamic modelo = new { item.IdProceso, item.Descripcion };
                    listaModeloFiltrado.Add(modelo);
                }
                
                return JsonConvert.SerializeObject(GestionAuxiliar.FormatoRespuesta(0, "éxito", new { listaModeloFiltrado}));
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-1, "Ha ocurrido un error en el servidor", new { error = ex.Message }));
            }
        }

        /// <summary>
        /// Crea un proceso por medio de un modelo
        /// </summary>
        /// <returns>Estado de la solicitud</returns>
        [HttpPut]
        public dynamic procesoByModelo(int idModelo) {
            int codigo;
            string mensaje;
            dynamic data = null;

            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            try
            {
                if (Proceso.procesoByModelo(idModelo))
                {
                    Response.StatusCode = 201;
                    codigo = 1;
                    mensaje = "agregado correctamente";
                }
                else
                {
                    Response.StatusCode = 404;
                    codigo = -1;
                    mensaje = "No fue posible crear el proceso, es posible que el ID no corresponda";
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                codigo = -2;
                mensaje = "Ha ocurrido un error en el servidor";
                data = new { error = ex.Message };
            }
            return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(codigo, mensaje, data));
        }
        #endregion

    }
}