﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProcessService.Auxiliar;
using Newtonsoft.Json;
using ProcessService.Models;
using System.Web;
using System.Web.Http.Cors;

namespace ProcessService.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    //[EnableCors(origins:"*",headers: "*", methods: "*")]
    public class CredencialesController : ApiController
    {
        [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
        [HttpPost]
        /// <summary>
        /// permite generar el login de conexión a la aplicación
        /// </summary>
        /// <returns>Token de respuesta con el detalle del usuario</returns>
        public object login([FromBody]dynamic credenciales) {
            
            try
            {
                string allowedOrigin = "http://localhost:3000/";
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", allowedOrigin);
                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET,POST");
                
                
                string usuario = credenciales["usuario"];
                string contrasenna = credenciales["contrasenna"];
                contrasenna = GestionAuxiliar.Md5(contrasenna);
                //string usuario = Newtonsoft.Json.JsonConvert.SerializeObject(credenciales["usuario"]);

                //if (ConexionGeneral.EntitiesConexiones.USUARIO.Any(e => e.NOMBREUSUARIO == usuario && e.CONTRASENNA == contrasenna))
                if (Usuario.ValidarUsuario(usuario, contrasenna) != null)
                {

                    //string resp = ConexionGeneral.EntitiesConexiones.USUARIO.First(e => e.NOMBREUSUARIO == usuario && e.CONTRASENNA == contrasenna).NOMBREUSUARIO;
                    Usuario _usuario = Usuario.ValidarUsuario(usuario, contrasenna);
                    string token = GestionAuxiliar.GenerateTokenJwt(_usuario.IdUsuario.ToString());
                    Token.AgregarToken(new Token(token, _usuario.IdUsuario));
                    Sesion sesion = new Sesion(1, token);
                    object data =
                        new
                        {
                            nombreUsuario = usuario,
                            idUsuario = _usuario.IdUsuario,
                            idPerfil = _usuario.PerfilID,
                            token
                        };
                    return GestionAuxiliar.FormatoRespuesta(1, "éxito", data);

                }
                else
                {
                    return GestionAuxiliar.FormatoRespuesta(-1, "usuario o contraseña inválidos");
                }
            }
            catch (Exception ex)
            {

                return GestionAuxiliar.FormatoRespuesta(-2, "error en el servidor", new { error = ex.Message });
            }
            
        }

        [HttpGet]
        public string[] tareas() {
            string[] ret = new string[4];
            ret[0] = "1";
            ret[1] = "2";
            ret[2] = "3";
            ret[3] = "4";
            return ret;
        }

    }
}
