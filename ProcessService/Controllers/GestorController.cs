﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProcessService.Models;
using Newtonsoft.Json;
using ProcessService.Auxiliar;
using System.Data.Entity;

namespace ProcessService.Controllers
{
    public class GestorController : ApiController
    {
        /// <summary>
        /// Lista los usuarios guardados en la base de datos
        /// </summary>
        /// <returns>lista de usuarios</returns>
        [HttpGet]
        public dynamic Usuarios()
        {
            try
            {
                List<Usuario> usuarios = new List<Usuario>();
                if (ConexionGeneral.EntitiesConexiones.USUARIO.Count() > 0)
                {
                    foreach (var item in ConexionGeneral.EntitiesConexiones.USUARIO)
                    {
                        usuarios.Add(new Usuario(
                            (int)item.ID_USUARIO,
                            item.NOMBREUSUARIO,
                            item.NOMBRE,
                            item.APELLIDOS,
                            item.CORREO,
                            (int)item.PERFIL_ID,
                            (int)item.CARGO_ID,
                            (int)item.EMPRESA_ID));
                    }
                }
                return usuarios;
                //JsonConvert.SerializeObject(Usuario.ListaUsuarios());
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        /// <summary>
        /// Retorna un usuario especifico según su ID
        /// </summary>
        /// <param name="idUsuario">ID de usuario a buscar</param>
        /// <returns>detalle del usuario o explicación de la excepción</returns>
        [HttpGet]
        public dynamic Usuario(int idUsuario)
        {
            int respCodigo = -1;
            dynamic _usuario = new { };
            Usuario usuario = null;
            string mensaje = "";
            string dataError = "";
            try
            {
                try
                {
                    _usuario = ConexionGeneral.EntitiesConexiones.USUARIO.First(usu => usu.ID_USUARIO == idUsuario);

                    usuario = new Usuario(
                            (int)_usuario.ID_USUARIO,
                            _usuario.NOMBREUSUARIO,
                            _usuario.NOMBRE,
                            _usuario.APELLIDOS,
                            _usuario.CORREO,
                            (int)_usuario.PERFIL_ID,
                            (int)_usuario.CARGO_ID,
                            (int)_usuario.EMPRESA_ID);

                    respCodigo = 0;
                    mensaje = "éxito";

                }
                catch (Exception ex)
                {
                    respCodigo = ex.Message == "La secuencia no contiene elementos" ? -1 : -2;
                    mensaje = respCodigo == -1 ? "el ID no corresponde a ningún usuario" : "Ocurrió un error en el servidor";
                    dataError = ex.Message;
                    
                }
            }
            catch (Exception ex)
            {
                respCodigo = -2;
                mensaje = "Ocurrió un error en el servidor";
                dataError = ex.Message;
            }

            return respCodigo == 0 ? 
                GestionAuxiliar.FormatoRespuesta(respCodigo, mensaje, JsonConvert.SerializeObject(usuario)) 
                : GestionAuxiliar.FormatoRespuesta(respCodigo, mensaje,new {mensaje=dataError});
        }

        /// <summary>
        /// Permite editar la información de un usuario
        /// </summary>
        /// <param name="dataUsuario"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic Usuario([FromBody] dynamic dataUsuario) {
            int respcod = -1;
            string mensaje = "",data ="";
            try
            {
                int idUsuario = dataUsuario.idUsuario;
                

                if (idUsuario != 0 && ConexionGeneral.EntitiesConexiones.USUARIO.Any(user => user.ID_USUARIO == idUsuario))
                {
                    ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).NOMBRE = 
                        dataUsuario.nombre != null ? 
                        dataUsuario.nombre : 
                        ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).NOMBRE;

                    ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).NOMBREUSUARIO =
                        dataUsuario.nombreUsuario != null ?
                        dataUsuario.nombreUsuario :
                        ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).NOMBREUSUARIO;

                    ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).APELLIDOS =
                        dataUsuario.apellidos != null ?
                        dataUsuario.apellidos :
                        ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).APELLIDOS;

                    ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).CORREO =
                        dataUsuario.correo != null ?
                        dataUsuario.correo :
                        ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario).CORREO;

                    respcod = 1;
                    mensaje = "éxito";
                }
            }
            catch (Exception ex)
            {
                respcod = ex.Message == "La secuencia no contiene elementos" ? -1 : -2;
                mensaje = respcod == -1?"el ID de usuario no existe en la base de datos":"ha ocurrido un error en la ejecución";
                data = ex.Message;
                throw;
            }

            return respcod == 1 ? GestionAuxiliar.FormatoRespuesta(respcod, mensaje) : GestionAuxiliar.FormatoRespuesta(respcod, mensaje, new { mensaje = data });
        }

        /// <summary>
        /// Permite agregar un usuario nuevo
        /// </summary>
        /// <param name="dataUsuario"></param>
        /// <returns></returns>
        [HttpPut]
        public dynamic AddUsuario([FromBody]dynamic dataUsuario) {
            int respCod = -1;
            string mensaje = "", data = "";


            try
            {
                string nombreUsuario = dataUsuario.nombreUsuario;
                if (nombreUsuario != null && !ConexionGeneral.EntitiesConexiones.USUARIO.Any(user => user.NOMBREUSUARIO == nombreUsuario))
                {
                    Conector.USUARIO _usuario = new Conector.USUARIO();
                    string contrasenna = dataUsuario.contrasenna;

                    _usuario.ID_USUARIO = ConexionGeneral.EntitiesConexiones.USUARIO.Max(user => user.ID_USUARIO) + 1;
                    _usuario.NOMBREUSUARIO = nombreUsuario;
                    _usuario.CONTRASENNA = GestionAuxiliar.Md5(contrasenna);
                    _usuario.NOMBRE = dataUsuario.nombre;
                    _usuario.APELLIDOS = dataUsuario.apellidos;
                    _usuario.CORREO = dataUsuario.correo;
                    _usuario.PERFIL_ID = dataUsuario.perfilID;
                    _usuario.CARGO_ID = dataUsuario.cargoID;
                    _usuario.EMPRESA_ID = dataUsuario.empresaID;

                    ConexionGeneral.EntitiesConexiones.USUARIO.Add(_usuario);
                    ConexionGeneral.EntitiesConexiones.SaveChanges();
                    respCod = 1;
                    mensaje = "éxito";   
                }
                else
                {
                    respCod = -1;
                    mensaje = "usuario ya existente";
                    data = "nombre de usuario ya existente";
                }
            }
            catch (Exception ex)
            {
                respCod = -2;
                mensaje = "ha ocurrido un error a nivel de servidor";
                data = ex.Message;
            }

            return respCod == 1 ? GestionAuxiliar.FormatoRespuesta(respCod, mensaje) : GestionAuxiliar.FormatoRespuesta(respCod, mensaje, data);
        }

        [HttpDelete]
        public dynamic DelUsuario([FromUri] int idUsuario) {
            int respCod = -1;
            string mensaje = "", data = "";


            try
            {
                if (ConexionGeneral.EntitiesConexiones.USUARIO.Any(user => user.ID_USUARIO == idUsuario))
                {
                    Conector.USUARIO _usuario = ConexionGeneral.EntitiesConexiones.USUARIO.First(user => user.ID_USUARIO == idUsuario);
                    ConexionGeneral.EntitiesConexiones.USUARIO.Remove(_usuario);
                    ConexionGeneral.EntitiesConexiones.SaveChanges();

                    respCod = 1;
                    mensaje = "éxito";
                }
                else
                {
                    respCod = -1;
                    mensaje = "el usuario indicado no existe en la base de datos";
                    data = mensaje;
                }
            }
            catch (Exception ex)
            {
                respCod = -2;
                mensaje = "ocurrió un error a nivel de servidor";
                data = ex.Message;
            }

            return respCod == 1 ? GestionAuxiliar.FormatoRespuesta(respCod, mensaje) : GestionAuxiliar.FormatoRespuesta(respCod, mensaje, data);

        }
    }
}
