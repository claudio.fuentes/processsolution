﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProcessService.Auxiliar;
using Newtonsoft.Json;
using ProcessService.Models;

namespace ProcessService.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public dynamic EmpresaCustom() {
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            string token = Request.Headers["login_access_token"];
            try
            {
                if (Token.ValidarToken(token))
                {
                    List<Empresa> empresas = Empresa.listaEmpresas();

                    Response.StatusCode = 200;
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(0, "éxito", empresas));
                }
                else
                {
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-1, "Token inválido"));
                }
            }
            catch (Exception ex)
            {
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-2, "ha ocurrido un error en la recuperación de la lista", new { errorServidor = ex.Message }));
            }
        }
    }
}