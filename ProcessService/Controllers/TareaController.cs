﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Newtonsoft.Json;
using ProcessService.Auxiliar;
using ProcessService.Models;

namespace ProcessService.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class TareaController : Controller
    {
        /// <summary>
        /// Lista las tareas asociadas al usuario
        /// </summary>
        /// <param name="idUsuario">id usuario al que recuperar las tareas</param>
        /// <returns>lista de las tareas</returns>
        [HttpPost][AllowCrossSiteJson][HttpOptions]
        public dynamic Tareas(int idUsuario) {
            string allowedOrigin = "http://localhost:3000";
            Response.AddHeader("Access-Control-Allow-Headers", GestionAuxiliar.JSONTYPE);
            Response.AddHeader("Access-Control-Allow-Origin", allowedOrigin);
            Response.AddHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT");
            
            
            
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            string token = Request.Headers[GestionAuxiliar.ACCESS_TOKEN_NAME];
            try
            {
                if (Token.ValidarToken(token, idUsuario))
                {
                    Response.StatusCode = 200;
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(0, "éxito", new { lista = Tarea.ListaTarea(idUsuario) }));
                }
                else
                {
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-1, "Token Inválido"));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-2, "Problema en el servidor", new { error = ex.Message }));
            }
        }

        /// <summary>
        /// Agrega una nueva tarea
        /// </summary>
        /// <param name="tarea">objecto tarea con la información</param>
        /// <returns>estado de la solicitud</returns>
        [HttpPut]
        public dynamic NuevaTarea(int idTarea, int nivel, int usuarioID, int procesoID, int estadoTareaID, string descripcion, string observaciones = null, DateTime? inicio = null, DateTime? termino = null, DateTime? inicioRegistrado = null, DateTime? terminoRegistrado = null, int? tareaMadreID = 0) {
            Request.ContentType = GestionAuxiliar.JSONTYPE;
            Response.ContentType = GestionAuxiliar.JSONTYPE;
            string token = Request.Headers[GestionAuxiliar.ACCESS_TOKEN_NAME];
            try
            {
                if (Token.ValidarToken(token,usuarioID))
                {
                    Tarea tarea = new Tarea(
                        idTarea, 
                        nivel, 
                        usuarioID, 
                        procesoID, 
                        estadoTareaID, 
                        descripcion, 
                        observaciones, 
                        inicio, 
                        termino, 
                        inicioRegistrado, 
                        terminoRegistrado,
                        tareaMadreID);
                    if (Tarea.AgregarTarea(tarea))
                    {
                        Response.StatusCode = 201;
                        return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(1, "éxito"));
                    }
                    Response.StatusCode = 500;
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-1, "No fue posible agregar la Tarea"));
                }
                else
                {
                    return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-4));
                }
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                return JsonConvert.SerializeObject(GestionAuxiliar.Respuesta(-2, "ha ocurrido un error en el servidor", new { error = ex.Message }));
            }
        }
    }
}