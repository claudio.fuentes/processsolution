﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Conector;

namespace ProcessService.Auxiliar
{
    public class ConexionGeneral
    {
        private static EntitiesConexiones _entitiesConexiones;

        public static EntitiesConexiones EntitiesConexiones 
        {
            get
            {
                if (_entitiesConexiones == null)
                {
                    _entitiesConexiones = new EntitiesConexiones();
                }
                return _entitiesConexiones;
            } 
        }

        public ConexionGeneral()
        {

        }
    }
}