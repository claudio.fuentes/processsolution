﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Net;
using Newtonsoft.Json;

namespace ProcessService.Auxiliar
{
    
    public static class GestionAuxiliar
    {
        /// <summary>formato de dato para envío y recepción</summary>
        public static string JSONTYPE = "application/json";
        /// <summary> nombre dado al token de envío</summary>
        public static string ACCESS_TOKEN_NAME = "login_access_token";

        /// <summary>
        /// Hashea valores con MD5
        /// </summary>
        /// <param name="Value">valor a Hashear</param>
        /// <returns>valor Hasheado</returns>
        public static string Md5(string Value)
        {
            MD5CryptoServiceProvider x = new MD5CryptoServiceProvider();
            byte[] data = System.Text.Encoding.ASCII.GetBytes(Value);
            data = x.ComputeHash(data);
            string ret = "";
            for (int i = 0; i < data.Length; i++)
                ret += data[i].ToString("x2").ToLower();
            return ret;
        }

        /// <summary>
        /// Crea una estructura de respuesta estandarizada
        /// </summary>
        /// <param name="codigo">número que corresponde al tipo de respuesta otorgada</param>
        /// <param name="mensaje">mnesaje para el usuario o desarrollador</param>
        /// <param name="data">paquete de datos a retornar</param>
        /// <returns>objeto JSON con todos los valores asignados</returns>
        public static object FormatoRespuesta(int codigo, string mensaje = null, object data = null)
        {
            if (data == null)
            {
                data = "";
            }
            object resp =
                new
                {
                    codigo,
                    mensaje,
                    data
                };
            return resp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="codigo">código de respuesta</param>
        /// <param name="mensaje"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object Respuesta(int codigo, string mensaje = null, object data = null)
        {
            switch (codigo)
            {
                case -4:
                    return new { codigo, mensaje = "token inválido", data };
                    break;
                default:
                    return new { codigo, mensaje, data };
                    break;
            }
            //return (codigo,mensaje,data);
        }

        /// <summary>
        /// Genera un token
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static string GenerateTokenJwt(string username)
        {
            // appsetting for Token JWT
            var secretKey = ConfigurationManager.AppSettings["JWT_SECRET_KEY"];
            var audienceToken = ConfigurationManager.AppSettings["JWT_AUDIENCE_TOKEN"];
            var issuerToken = ConfigurationManager.AppSettings["JWT_ISSUER_TOKEN"];
            var expireTime = ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"];

            var securityKey = new SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(secretKey));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);

            // create a claimsIdentity
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, username) });

            // create token to the user
            var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var jwtSecurityToken = tokenHandler.CreateJwtSecurityToken(
                audience: audienceToken,
                issuer: issuerToken,
                subject: claimsIdentity,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddMinutes(Convert.ToInt32(expireTime)),
                signingCredentials: signingCredentials);

            var jwtTokenString = tokenHandler.WriteToken(jwtSecurityToken);
            return jwtTokenString;
        }


        
    }
}