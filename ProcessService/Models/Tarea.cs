﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessService.Auxiliar;
using Conector;

namespace ProcessService.Models
{
    public class Tarea
    {
        int idTarea, nivel, usuarioID, procesoID, estadoTareaID, tareaMadreID;
        DateTime? inicio, termino, inicioRegistrado, terminoRegistrado;
        string descripcion, observaciones;

        public Tarea(int idTarea, int nivel, int usuarioID, int procesoID, int estadoTareaID, string descripcion, string observaciones = null, DateTime? inicio = null, DateTime? termino = null, DateTime? inicioRegistrado = null, DateTime? terminoRegistrado = null, int? tareaMadreID = null)
        {
            this.idTarea = idTarea;
            this.nivel = nivel;
            this.usuarioID = usuarioID;
            this.procesoID = procesoID;
            this.estadoTareaID = estadoTareaID;
            this.tareaMadreID = (int)tareaMadreID;
            this.inicio = inicio;
            this.termino = termino;
            this.inicioRegistrado = inicioRegistrado;
            this.terminoRegistrado = terminoRegistrado;
            this.descripcion = descripcion;
            this.observaciones = observaciones;
        }

        public int IdTarea { get => idTarea; set => idTarea = value; }
        public int Nivel { get => nivel; set => nivel = value; }
        public int UsuarioID { get => usuarioID; set => usuarioID = value; }
        public int ProcesoID { get => procesoID; set => procesoID = value; }
        public int EstadoTareaID { get => estadoTareaID; set => estadoTareaID = value; }
        public int TareaMadreID { get => tareaMadreID; set => tareaMadreID = value; }
        public DateTime? Inicio { get => inicio; set => inicio = value; }
        public DateTime? Termino { get => termino; set => termino = value; }
        public DateTime? InicioRegistrado { get => inicioRegistrado; set => inicioRegistrado = value; }
        public DateTime? TerminoRegistrado { get => terminoRegistrado; set => terminoRegistrado = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Observaciones { get => observaciones; set => observaciones = value; }


        /// <summary>
        /// Lista todas las tareas
        /// </summary>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea() {
            try
            {
                int _idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _tareaMadreID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConexionGeneral.EntitiesConexiones.TAREA)
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));

                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// Lista todas las tareas según usuario
        /// </summary>
        /// <param name="idUsuario">id usuario</param>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea(int idUsuario)
        {
            try
            {
                int _idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _tareaMadreID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConexionGeneral.EntitiesConexiones.TAREA.Where(hw => hw.USUARIO_ID == idUsuario))
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));

                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Lista todas las tareas según usuario
        /// </summary>
        /// <param name="idUsuario">id usuario</param>
        /// <param name="idProceso">id proceso</param>
        /// <returns>Lista de tareas</returns>
        public static List<Tarea> ListaTarea(int idUsuario, int idProceso)
        {
            try
            {
                int _idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _tareaMadreID;
                DateTime? _inicio, _termino, _inicioRegistrado, _terminoRegistrado;
                string _descripcion, _observaciones;
                List<Tarea> _listaTarea = new List<Tarea>();

                foreach (var item in ConexionGeneral.EntitiesConexiones.TAREA.Where(hw => hw.USUARIO_ID == idUsuario && hw.PROCESO_ID == idProceso))
                {
                    _idTarea = (int)item.ID_TAREA;
                    _nivel = (int)item.NIVEL;
                    _usuarioID = (int)item.USUARIO_ID;
                    _procesoID = (int)item.PROCESO_ID;
                    _estadoTareaID = (int)item.ESTADOTAREA_ID;
                    _tareaMadreID = (int)item.TAREAMADRE;
                    _inicio = item.INICIO;
                    _termino = item.TERMINO;
                    _inicioRegistrado = item.INICIOREGISTRADO;
                    _terminoRegistrado = item.TERMINOREGISTRADO;
                    _descripcion = item.DESCRIPCION;
                    _observaciones = item.OBSERVACIONES;

                    _listaTarea.Add(new Tarea(_idTarea, _nivel, _usuarioID, _procesoID, _estadoTareaID, _descripcion, _observaciones, _inicio,
                        _termino, _inicioRegistrado, _terminoRegistrado, _tareaMadreID));
                }

                return _listaTarea;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Agrega una nueva tarea al sistema
        /// </summary>
        /// <param name="_tarea">objeto tarea</param>
        /// <returns>estado del inserto</returns>
        public static bool AgregarTarea(Tarea _tarea) {
            try
            {
                TAREA nuevaTarea = new TAREA();
                int idTarea;
                try
                {
                    idTarea = (int)ConexionGeneral.EntitiesConexiones.TAREA.Max(hw => hw.ID_TAREA);
                }
                catch (Exception)
                {
                    idTarea = 0;
                }
                

                nuevaTarea.ID_TAREA = (decimal)idTarea + 1;
                nuevaTarea.DESCRIPCION = _tarea.Descripcion;
                nuevaTarea.ESTADOTAREA_ID = _tarea.EstadoTareaID;
                nuevaTarea.OBSERVACIONES = _tarea.Observaciones;
                nuevaTarea.INICIO = _tarea.Inicio;
                nuevaTarea.TERMINO = _tarea.Termino;
                nuevaTarea.NIVEL = _tarea.Nivel;
                nuevaTarea.USUARIO_ID = _tarea.UsuarioID;
                nuevaTarea.PROCESO_ID = _tarea.ProcesoID;
                nuevaTarea.INICIOREGISTRADO = _tarea.InicioRegistrado;
                nuevaTarea.TERMINOREGISTRADO = _tarea.TerminoRegistrado;
                nuevaTarea.TAREAMADRE = _tarea.TareaMadreID;
                nuevaTarea.TAREAMADRE = _tarea.TareaMadreID!=0? _tarea.TareaMadreID : (decimal)idTarea + 1;

                ConexionGeneral.EntitiesConexiones.TAREA.Add(nuevaTarea);
                ConexionGeneral.EntitiesConexiones.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}