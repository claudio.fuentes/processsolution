﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Conector;
using ProcessService.Auxiliar;

namespace ProcessService.Models
{
    public class Usuario
    {
        int idUsuario;
        string nombreUsuario;
        string nombre;
        string apellido;
        string correo;
        int perfilID, cargoID, empresaID;

        public Usuario(int idUsuario, string nombreUsuario, string nombre, string apellido, string correo, int perfilID, int cargoID, int empresaID)
        {
            this.IdUsuario = idUsuario;
            this.NombreUsuario = nombreUsuario;
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Correo = correo;
            this.PerfilID = perfilID;
            this.CargoID = cargoID;
            this.EmpresaID = empresaID;
        }

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Correo { get => correo; set => correo = value; }
        public int PerfilID { get => perfilID; set => perfilID = value; }
        public int CargoID { get => cargoID; set => cargoID = value; }
        public int EmpresaID { get => empresaID; set => empresaID = value; }

        public static Usuario ValidarUsuario(string nombre,string contrasenna) {
            try
            {
                USUARIO _usuario = ConexionGeneral.EntitiesConexiones.USUARIO.First(usu => usu.CONTRASENNA == contrasenna && usu.NOMBREUSUARIO == nombre);

                if (_usuario != null)
                {
                    return new Usuario(
                        (int)_usuario.ID_USUARIO,
                        _usuario.NOMBREUSUARIO,
                        _usuario.NOMBRE,
                        _usuario.APELLIDOS,
                        _usuario.CORREO,
                        (int)_usuario.PERFIL_ID,
                        (int)_usuario.CARGO_ID,
                        (int)_usuario.EMPRESA_ID);
                }
                else
                {
                    return null;
                }
               
            }
            catch (Exception ex)
            {

                return null;
            }
        
        }

        //public static List<USUARIO> ListaUsuarios() {
        //    List<USUARIO> lRet = new List<USUARIO>();
        //    if (ConexionGeneral.EntitiesConexiones.USUARIO.Count() > 0)
        //    {
        //        foreach (var item in ConexionGeneral.EntitiesConexiones.USUARIO)
        //        {
        //            lRet.Add(item);
        //        }
        //        return lRet;

        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
    }
}