﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Conector;
using ProcessService.Auxiliar;

namespace ProcessService.Models
{
    public class Empresa
    {
        int idEmpresa;
        string razonSocial;

        public Empresa(int idEmpresa, string razonSocial)
        {
            this.IdEmpresa = idEmpresa;
            this.RazonSocial = razonSocial;
        }

        public int IdEmpresa { get => idEmpresa; set => idEmpresa = value; }
        public string RazonSocial { get => razonSocial; set => razonSocial = value; }

        public static List<Empresa> listaEmpresas() {
            try
            {
                DbSet<EMPRESA> empresas = ConexionGeneral.EntitiesConexiones.EMPRESA;
                List<Empresa> listaEmpresas = new List<Empresa>();

                foreach (var item in empresas)
                {
                    int _idEmpresa = (int)item.ID_EMPRESA;
                    string _razonSocial = item.RAZONSOCIAL;

                    listaEmpresas.Add(new Empresa(_idEmpresa, _razonSocial));
                }

                return listaEmpresas;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}