﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Conector;
using ProcessService.Auxiliar;

namespace ProcessService.Models
{
    public class Token
    {
        /// <summary>
        /// valor del token
        /// </summary>
        string valor;
        /// <summary>
        /// tiempo de expiración en minutos del token
        /// </summary>
        DateTime expiracion;
        /// <summary>
        /// usuario asociado al token
        /// </summary>
        int idUsuario;

        /// <summary>
        /// Crea una nueva instancia de Token
        /// </summary>
        /// <param name="valor">valor del token</param>
        /// <param name="expiracion">tiempo de expiración del token en minutos</param>
        /// <param name="idUsuario">usuario asociado al token</param>
        public Token(string valor, int expiracion, int idUsuario)
        {
            this.Valor = valor;
            this.Expiracion = DateTime.Now.AddMinutes(expiracion);
            this.IdUsuario = idUsuario;
        }

        /// <summary>
        /// Crea una nueva instancia de Token
        /// </summary>
        /// <param name="valor">valor del token</param>
        /// <param name="idUsuario">usuario asociado al token</param>
        public Token(string valor, int idUsuario)
        {
            this.Valor = valor;
            this.Expiracion = DateTime.Now.AddMinutes(Double.Parse(ConfigurationManager.AppSettings["JWT_EXPIRE_MINUTES"]));
            this.IdUsuario = idUsuario;
        }

        public string Valor { get => valor; set => valor = value; }
        public DateTime Expiracion { get => expiracion; set => expiracion = value; }
        public int IdUsuario { get => idUsuario; set => idUsuario = value; }

        /// <summary>
        /// Método estático que almacena el nuevo token en la base de datos.
        /// </summary>
        /// <param name="_token">Token con toda la información necesaria</param>
        /// <returns>estado de la consulta</returns>
        public static bool AgregarToken(Token _token) {
            try
            {
                TOKEN_REGISTRO registro = new TOKEN_REGISTRO();
                registro.TOKEN = _token.Valor;
                registro.USUARIO_ID = _token.IdUsuario;
                registro.VENCIMIENTO = _token.Expiracion;
                registro.ID_TOKEN = ConexionGeneral.EntitiesConexiones.TOKEN_REGISTRO.Max(tkn => tkn.ID_TOKEN) + 1; 
                ConexionGeneral.EntitiesConexiones.TOKEN_REGISTRO.Add(registro);
                try
                {
                    ConexionGeneral.EntitiesConexiones.SaveChanges();
                }
                catch (Exception ex)
                {

                    throw;
                }
                

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Valida si el token se encuentra registrada y activo en la base de datos.
        /// </summary>
        /// <param name="_token">token a validar</param>
        /// <returns>estado del token</returns>
        public static bool ValidarToken(string _token) {
            try
            {
                return ConexionGeneral.EntitiesConexiones.TOKEN_REGISTRO.First(tkn => tkn.TOKEN == _token && tkn.VENCIMIENTO >= DateTime.Now) != null ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ValidarToken(string _token, int idUsuario)
        {
            try
            {
                return ConexionGeneral.EntitiesConexiones.TOKEN_REGISTRO.First(tkn => tkn.TOKEN == _token && tkn.VENCIMIENTO >= DateTime.Now && tkn.USUARIO_ID == idUsuario) != null ? true : false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}