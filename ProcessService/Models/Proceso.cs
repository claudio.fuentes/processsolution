﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProcessService.Auxiliar;
using System.Data.Entity;
using Conector;

namespace ProcessService.Models
{
    /// <summary>
    /// Procesos de trabajo
    /// </summary>
    public class Proceso
    {
        int idProceso,idEmpresa;
        string descripcion, detalle;
        bool modelo;
        DateTime inicio;
        DateTime? termino;

        /// <summary>
        /// Constructor Proceso
        /// </summary>
        /// <param name="idProceso"></param>
        /// <param name="idEmpresa"></param>
        /// <param name="descripcion"></param>
        /// <param name="detalle"></param>
        /// <param name="modelo"></param>
        /// <param name="inicio"></param>
        /// <param name="termino"></param>
        public Proceso(int idProceso, int idEmpresa, string descripcion, string detalle, bool modelo, DateTime inicio, DateTime? termino)
        {
            this.IdProceso = idProceso;
            this.IdEmpresa = idEmpresa;
            this.Descripcion = descripcion;
            this.Detalle = detalle;
            this.Modelo = modelo;
            this.Inicio = inicio;
            this.Termino = termino;
        }


        public int IdProceso { get => idProceso; set => idProceso = value; }
        public int IdEmpresa { get => idEmpresa; set => idEmpresa = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Detalle { get => detalle; set => detalle = value; }
        public bool Modelo { get => modelo; set => modelo = value; }
        public DateTime Inicio { get => inicio; set => inicio = value; }
        public DateTime? Termino { get => termino; set => termino = value; }


        #region MÉTODOS ESTÁTICOS
        /// <summary>
        /// lista los procesos con toda su información
        /// </summary>
        /// <returns>lista de procesos</returns>
        public static List<Proceso> listaProcesoCompleto()
        {
            try
            {
                List<Proceso> listaProcesos = new List<Proceso>();
                DbSet<PROCESO> procesos = ConexionGeneral.EntitiesConexiones.PROCESO;

                foreach (var item in procesos)
                {
                    int _idProceso, _idEmpresa;
                    string _descripcion, _detalle;
                    bool _modelo;
                    DateTime _inicio;
                    DateTime? _termino;

                    _idProceso = (int)item.ID_PROCESO;
                    _idEmpresa = (int)item.EMPRESA_ID;
                    _descripcion = item.DESCRIPCION;
                    _detalle = item.DETALLE;
                    _modelo = item.MODELO == "1" ? true : false;
                    _inicio = item.INICIO;
                    _termino = (DateTime?)item.TERMINO;

                    listaProcesos.Add(new Proceso(_idProceso, _idEmpresa, _descripcion, _detalle, _modelo, _inicio, _termino));
                }


                return listaProcesos;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// Retorna la información especifica para las vistas donde se requiere la lista de procesos
        /// </summary>
        /// <returns>lista customizada</returns>
        public static List<dynamic> listaCustomizada() {
            try
            {
                DbSet<PROCESO> _proceso = ConexionGeneral.EntitiesConexiones.PROCESO;
                List<dynamic> listaProcesosCustom = new List<dynamic>();


                foreach (var item in _proceso)
                {
                    if (item.MODELO == "0")
                    {
                        dynamic procesoCustom =
                        new
                        {
                            idProceso = (int)item.ID_PROCESO,
                            empresa = item.EMPRESA.RAZONSOCIAL,
                            proceso = item.DESCRIPCION,
                            inicio = item.INICIO,
                            termino = item.TERMINO,
                            tareas = item.TAREA.Count,
                            tareasFinalizadas = item.TAREA.Where(tar => tar.TERMINOREGISTRADO != null).Count()
                        };
                        listaProcesosCustom.Add(procesoCustom);
                    }
                }
                return listaProcesosCustom;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Agrega un proceso a la base de datos
        /// </summary>
        /// <param name="proceso">proceso a agregar</param>
        /// <returns>true si se agregó exitosamente</returns>
        public static bool agregaProceso(Proceso proceso) {
            try
            {
                PROCESO _proceso = new PROCESO();
                _proceso.DESCRIPCION = proceso.Descripcion;
                _proceso.DETALLE = proceso.Detalle;
                _proceso.EMPRESA_ID = proceso.IdEmpresa;
                _proceso.ID_PROCESO = proceso.IdProceso = ConexionGeneral.EntitiesConexiones.PROCESO.Max(prs => (int)prs.ID_PROCESO) + 1;
                _proceso.INICIO = proceso.Inicio;
                _proceso.MODELO = proceso.Modelo ? "1" : "0";
                _proceso.TERMINO = proceso.Termino;
                ConexionGeneral.EntitiesConexiones.PROCESO.Add(_proceso);

                ConexionGeneral.EntitiesConexiones.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Proceso buscarPorID(int idProceso) {
            try
            {
                DbSet<PROCESO> resultado = ConexionGeneral.EntitiesConexiones.PROCESO;
                foreach (var item in resultado)
                {
                    var algo = item.ID_PROCESO;
                }
                PROCESO _proceso = resultado.First(pr => pr.ID_PROCESO == idProceso);

                if (_proceso != null)
                {
                    return
                    new Proceso(
                        idProceso,
                        (int)_proceso.EMPRESA_ID,
                        _proceso.DESCRIPCION,
                        _proceso.DETALLE,
                        _proceso.MODELO == "1" ? true : false,
                        _proceso.INICIO,
                        _proceso.TERMINO
                        );
                }
                else
                {
                    return null;
                }
                
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool procesoByModelo(int idProceso) {
            try
            {
                Proceso _procesoModelo = buscarPorID(idProceso);
                if (_procesoModelo.Modelo)
                {
                    _procesoModelo.Modelo = false;
                    agregaProceso(_procesoModelo);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion


    }
}