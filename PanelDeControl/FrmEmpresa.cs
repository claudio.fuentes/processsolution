﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;
using PanelDeControl.frmNuevos;
using PanelDeControl.frmEditar;

namespace PanelDeControl
{
    public partial class FrmEmpresa : Form
    {
        #region PROPIEDADES
        DataTable dttEmpresas = new DataTable();
        #endregion


        #region INSTANCIADORES E INICIALIZADORES
        public FrmEmpresa()
        {
            InitializeComponent();
            InstanciadorDGV();
        }
        public void InstanciadorDGV() {
            try
            {
                dttEmpresas.Columns.Add("idEmpresa");
                dttEmpresas.Columns.Add("Razón Social");

                dttEmpresas.Columns[1].ColumnName = "razonSocial";

                dttEmpresas.Columns[0].ColumnMapping = MappingType.Hidden;

                CargaDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }        
        }
        public void CargaDGV()
        {
            try
            {
                dttEmpresas.Rows.Clear();
                foreach (Empresa item in Empresa.listaEmpresas())
                {
                    object[] empresa = new object[]
                    {
                        item.IdEmpresa,
                        item.RazonSocial
                    };
                    dttEmpresas.Rows.Add(empresa);
                }

                dgvEmpresas.DataSource = dttEmpresas;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region MÉTODOS

        #endregion

        #region ACCIÓN DE CONTROLES
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmAddEmpresa frmAddEmpresa = new FrmAddEmpresa();
            frmAddEmpresa.ShowDialog();
            CargaDGV();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                

                var tablaFila = dgvEmpresas.SelectedRows[0];
                var dataFilaId = dttEmpresas.Rows[tablaFila.Index][0];
                string dataFilaName = dttEmpresas.Rows[tablaFila.Index][1].ToString();

                DialogResult dialogResult = MessageBox.Show($"¿está seguro que desea eliminar la empresa {dataFilaName}?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                int usuariosAsociados = 0;
                int procesosAsociados = 0;

                Empresa.UsuariosYProcesosASociados(int.Parse(dataFilaId.ToString())).TryGetValue("usuario", out usuariosAsociados);
                Empresa.UsuariosYProcesosASociados(int.Parse(dataFilaId.ToString())).TryGetValue("procesos", out procesosAsociados);
                if (usuariosAsociados > 0 || procesosAsociados > 0)
                {
                    MessageBox.Show($"No es posible eliminar la empresa ya que esta posee las siguientes asociaciones con otras tablas:\n{usuariosAsociados} usuarios\n{procesosAsociados} procesos\nprocure que la empresa no tenga estas asociaciones y luego intente eliminarla de nuevo", "dvertencia", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }

                if (Empresa.EliminarEmpresa(int.Parse(dataFilaId.ToString())))
                {
                    MessageBox.Show($"La empresa \"{dataFilaName}\" ha sido eliminada exitosamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    CargaDGV();
                }
                else
                {
                    MessageBox.Show($"No ha sido posible eliminar la empresa seleccionada", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                var tablaFila = dgvEmpresas.SelectedRows[0];
                int dataFilaId = int.Parse(dttEmpresas.Rows[tablaFila.Index][0].ToString());
                string dataFilaName = dttEmpresas.Rows[tablaFila.Index][1].ToString();

                Empresa empresaSeleccionada = new Empresa(dataFilaId, dataFilaName);
                FrmEditEmpresa frmEditEmpresa = new FrmEditEmpresa(empresaSeleccionada);
                frmEditEmpresa.Owner = this;
                frmEditEmpresa.ShowDialog(this);
                CargaDGV();

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        #endregion


    }
}
