﻿namespace PanelDeControl
{
    partial class FrmCargo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
            private void InitializeComponent()
            {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCargo));
            this.tlp0All = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Dgv = new System.Windows.Forms.TableLayoutPanel();
            this.dgvCargo = new System.Windows.Forms.DataGridView();
            this.tlp1Botonera = new System.Windows.Forms.TableLayoutPanel();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.tlp0All.SuspendLayout();
            this.tlp1Dgv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCargo)).BeginInit();
            this.tlp1Botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp0All
            // 
            this.tlp0All.ColumnCount = 1;
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp0All.Controls.Add(this.tlp1Dgv, 0, 0);
            this.tlp0All.Controls.Add(this.tlp1Botonera, 0, 1);
            this.tlp0All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp0All.Location = new System.Drawing.Point(0, 0);
            this.tlp0All.Margin = new System.Windows.Forms.Padding(4);
            this.tlp0All.Name = "tlp0All";
            this.tlp0All.RowCount = 2;
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp0All.Size = new System.Drawing.Size(493, 631);
            this.tlp0All.TabIndex = 0;
            // 
            // tlp1Dgv
            // 
            this.tlp1Dgv.ColumnCount = 1;
            this.tlp1Dgv.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Dgv.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp1Dgv.Controls.Add(this.dgvCargo, 0, 0);
            this.tlp1Dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Dgv.Location = new System.Drawing.Point(3, 3);
            this.tlp1Dgv.Name = "tlp1Dgv";
            this.tlp1Dgv.RowCount = 1;
            this.tlp1Dgv.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Dgv.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 498F));
            this.tlp1Dgv.Size = new System.Drawing.Size(487, 498);
            this.tlp1Dgv.TabIndex = 0;
            // 
            // dgvCargo
            // 
            this.dgvCargo.AllowUserToAddRows = false;
            this.dgvCargo.AllowUserToDeleteRows = false;
            this.dgvCargo.AllowUserToResizeColumns = false;
            this.dgvCargo.AllowUserToResizeRows = false;
            this.dgvCargo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCargo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCargo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCargo.Location = new System.Drawing.Point(3, 3);
            this.dgvCargo.MultiSelect = false;
            this.dgvCargo.Name = "dgvCargo";
            this.dgvCargo.ReadOnly = true;
            this.dgvCargo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCargo.Size = new System.Drawing.Size(481, 492);
            this.dgvCargo.TabIndex = 0;
            // 
            // tlp1Botonera
            // 
            this.tlp1Botonera.ColumnCount = 3;
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp1Botonera.Controls.Add(this.btnEliminar, 2, 0);
            this.tlp1Botonera.Controls.Add(this.btnEditar, 1, 0);
            this.tlp1Botonera.Controls.Add(this.btnAgregar, 0, 0);
            this.tlp1Botonera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Botonera.Location = new System.Drawing.Point(3, 507);
            this.tlp1Botonera.Name = "tlp1Botonera";
            this.tlp1Botonera.RowCount = 1;
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp1Botonera.Size = new System.Drawing.Size(487, 121);
            this.tlp1Botonera.TabIndex = 1;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminar.Location = new System.Drawing.Point(343, 38);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(124, 45);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEditar.Location = new System.Drawing.Point(181, 38);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(124, 45);
            this.btnEditar.TabIndex = 1;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAgregar.Location = new System.Drawing.Point(19, 38);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(124, 45);
            this.btnAgregar.TabIndex = 0;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // FrmCargo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 631);
            this.Controls.Add(this.tlp0All);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FrmCargo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestor de Cargo";
            this.tlp0All.ResumeLayout(false);
            this.tlp1Dgv.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCargo)).EndInit();
            this.tlp1Botonera.ResumeLayout(false);
            this.ResumeLayout(false);

            }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp0All;
        private System.Windows.Forms.TableLayoutPanel tlp1Dgv;
        private System.Windows.Forms.DataGridView dgvCargo;
        private System.Windows.Forms.TableLayoutPanel tlp1Botonera;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEditar;
    }
}