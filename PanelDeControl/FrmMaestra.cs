﻿using PanelDeControl.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;

namespace PanelDeControl
{
    public partial class FrmMaestra : Form
    {
        //frmLogin login;
        public bool usuarioValido;
        Clases.Sesion sesion;
        bool formCargado = false;
        DataTable dttUsuarios;
        DataTable dttInformeEmpresas,dttInformeProcesos;
        int nvlInforme = 0;
        Empresa informeEmpresaSeleccionada = null;
        Proceso informeProcesoSeleccionado = null;


        internal Clases.Sesion Sesion { get => sesion; set => sesion = value; }

        public FrmMaestra()
        {
            InitializeComponent();
            
            CredencialesAcceso();
            CargaFormulario();
            CargaInformeInicial();
        }
        

        #region CARGAS INICIALES
        /// <summary>
        /// Gestiona el inicio de sesión
        /// </summary>
        private void CredencialesAcceso()
        {
            Sesion = null;
            frmLogin login = new frmLogin();
            login.Owner = this;
            btnLogin.Text = "iniciar sesión";
            tclPrincipal.Hide();
            
            login.ShowDialog();
            
            if (sesion != null)
            {
                lblUsuario.Text = sesion.NombreUsuario;
                btnLogin.Text = "cerrar sesión";
                tclPrincipal.Show();
            }
            else
            {
                Close();
            }
        }

        //GESTORES

        /// <summary>
        /// Ejecuta los procesos asociados al inicio del formulario
        /// </summary>
        private void CargaFormulario()
        {
            formCargado = false;
            CargaUsuarios();
            CargarCombobox();
            formCargado = true;
        }
        
        /// <summary>
        /// Carga todos los usuarios en la grilla
        /// 
        /// </summary>
        private void CargaUsuarios()
        {
            try
            {
                List<Usuario> listaUsuario = Usuario.ListaUsuarios();
                List<dynamic> listaUsuarioGrilla = new List<dynamic>();
                dttUsuarios = new DataTable();

                dttUsuarios.Columns.Add("idUsuario");
                dttUsuarios.Columns.Add("nombreUsuario");
                dttUsuarios.Columns.Add("nombre");
                dttUsuarios.Columns.Add("apellido");
                dttUsuarios.Columns.Add("correo");
                dttUsuarios.Columns.Add("perfilID");
                dttUsuarios.Columns.Add("perfilNombre");
                dttUsuarios.Columns.Add("cargoID");
                dttUsuarios.Columns.Add("cargoNombre");
                dttUsuarios.Columns.Add("empresaID");
                dttUsuarios.Columns.Add("empresaNombre");

                dttUsuarios.Columns["nombreUsuario"].ColumnName = "usuario";
                dttUsuarios.Columns["perfilNombre"].ColumnName = "perfil";
                dttUsuarios.Columns["cargoNombre"].ColumnName = "cargo";
                dttUsuarios.Columns["empresaNombre"].ColumnName = "empresa";

                dttUsuarios.Columns["idUsuario"].ColumnMapping = MappingType.Hidden;
                dttUsuarios.Columns["perfilID"].ColumnMapping = MappingType.Hidden;
                dttUsuarios.Columns["cargoID"].ColumnMapping = MappingType.Hidden;
                dttUsuarios.Columns["empresaID"].ColumnMapping = MappingType.Hidden;

                foreach (var item in listaUsuario)
                {
                    object[] data = new object[]
                    {
                        item.IdUsuario,
                        item.NombreUsuario,
                        item.Nombre,
                        item.Apellido,
                        item.Correo,
                        Perfil.perfilByID(item.PerfilID).IdPerfil,
                        Perfil.perfilByID(item.PerfilID).Descripcion,
                        Cargo.CargoByID(item.CargoID).IdCargo,
                        Cargo.CargoByID(item.CargoID).Descripcion,
                        Empresa.EmpresaByID(item.EmpresaID).IdEmpresa,
                        Empresa.EmpresaByID(item.EmpresaID).RazonSocial
                    };

                    dttUsuarios.Rows.Add(data);
                }
                dgvUsuario.DataSource = dttUsuarios;

            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// llena los combobox con la información necesaria
        /// </summary>
        private void CargarCombobox()
        {
            try
            {
                cmbUsu_perfil.DataSource = Perfil.listaPerfiles();
                cmbUsu_perfil.DisplayMember = "Descripcion";
                cmbUsu_perfil.ValueMember = "IdPerfil";
                cmbUsu_perfil.SelectedItem = null;

                cmbUsu_empresa.DataSource = Empresa.listaEmpresas();
                cmbUsu_empresa.DisplayMember = "RazonSocial";
                cmbUsu_empresa.ValueMember = "IdEmpresa";
                cmbUsu_empresa.SelectedItem = null;

                cmbUsu_Cargo.DataSource = Cargo.listaCargos();
                cmbUsu_Cargo.DisplayMember = "Descripcion";
                cmbUsu_Cargo.ValueMember = "IdCargo";
                cmbUsu_Cargo.SelectedItem = null;

                cmbRangoCrt1.DataSource = null;
                cmbRangoCrt1.Items.Add(new { id = 0, value = "7 días" });
                cmbRangoCrt1.Items.Add(new { id = 1, value = "30 días" });
                cmbRangoCrt1.Items.Add(new { id = 2, value = "2 meses" });
                cmbRangoCrt1.DisplayMember = "value";
                cmbRangoCrt1.ValueMember = "id";
                cmbRangoCrt1.SelectedIndex = 0;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        // INFORMES
         
        /// <summary>
        /// Carga los datos asociados a los informes y reportes
        /// </summary>
        private void CargaInformeInicial()
        {
            nvlInforme = 0;
            CargaDGVInforme(0);
            CargaCrtInformeCrt2(0);
            CargaCrtInformeCrt1(0);

            dgvInforme.Rows[0].Selected = true;
        }

        private void CargaDGVInforme(int nivel,int idEmpresa = 0)
        {
            dgvInforme.DataSource = null;
            formCargado = false;

            switch (nivel)
            {
                case 0:
                    dttInformeEmpresas = new DataTable();

                    dttInformeEmpresas.Columns.Add("idEmpresa");
                    dttInformeEmpresas.Columns.Add("Razón Social");

                    dttInformeEmpresas.Columns[0].ColumnMapping = MappingType.Hidden;

                    dttInformeEmpresas.Columns[1].ColumnName = "razonSocial";

                    dttInformeEmpresas.Rows.Add(new object[] { 0, "*Todas*" });

                    foreach (Empresa item in Empresa.listaEmpresas())
                    {
                        dttInformeEmpresas.Rows.Add(new object[] {
                            item.IdEmpresa,
                            item.RazonSocial
                        });
                    }

                    dgvInforme.DataSource = dttInformeEmpresas;
                    
                    break;
                case 1:
                    dttInformeProcesos = new DataTable();

                    dttInformeProcesos.Columns.Add("idProceso");
                    dttInformeProcesos.Columns.Add("Proceso");

                    dttInformeProcesos.Columns[0].ColumnMapping = MappingType.Hidden;

                    dttInformeProcesos.Columns[1].ColumnName = "descripcion";

                    dttInformeProcesos.Rows.Add(new object[] { 0, "*Todos*" });

                    foreach (Proceso item in Proceso.listaProcesoCompleto().Where(lp => lp.IdEmpresa == idEmpresa))
                    {
                        dttInformeProcesos.Rows.Add(new object[] {
                            item.IdProceso,
                            item.Descripcion
                        });
                    }

                    dgvInforme.DataSource = dttInformeProcesos;
                    break;
                default:
                    break;
            }

            foreach (DataGridViewColumn item in dgvInforme.Columns)
            {
                item.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            formCargado = true;
        }

        private void CambioEstadoReporte()
        {
            int periodo = cmbRangoCrt1.SelectedIndex;
            if (nvlInforme == 0)
            {
                informeEmpresaSeleccionada = SeleccionDGVInformesEmpresa();
                if (informeEmpresaSeleccionada == null)
                {
                    CargaCrtInformeCrt1(0,0,0,periodo);
                }
                else
                {
                    CargaCrtInformeCrt1(1, (int)informeEmpresaSeleccionada.IdEmpresa,0,periodo);
                }
            }
            else if(nvlInforme == 1)
            {

                informeProcesoSeleccionado = SeleccionDGVInformesProceso();
                if (informeProcesoSeleccionado == null)
                {
                    CargaCrtInformeCrt1(1, (int)informeEmpresaSeleccionada.IdEmpresa,0,periodo);
                }
                else
                {
                    CargaCrtInformeCrt1(2, 0, informeProcesoSeleccionado.IdProceso, periodo);
                }
                //TODO: implementar nivel 1 con las caracteristicas del if en 1
            }
        }

        private void CargaCrtInformeCrt2(int nivel,int IdEmpresa = 0)
        {
            crt2.DataSource = null;
            crt2.Series.Clear();
            formCargado = false;

            switch (nivel)
            {
                case 0:

                    crt2.Text = "Todos";
                    
                    crt2.Series.Add("Duoc");
                    crt2.Series.Add("Process");

                    crt2.Series[0].Points.AddXY("pendientes",15);
                    crt2.Series[1].Points.AddY(10);

                    crt2.Series[0].Points.AddXY("en proceso", 20);
                    crt2.Series[1].Points.AddY(10);

                    crt2.Series[0].Points.AddXY("finalizado", 10);
                    crt2.Series[1].Points.AddY(20);

                    crt2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    crt2.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    
                    break;
                case 1:
                    crt2.Text = "Process";

                    crt2.Series.Add("Contabilidad");
                    crt2.Series.Add("Recursos Humanos");
                    crt2.Series.Add("Informática");


                    crt2.Series[0].Points.AddXY("pendientes", 5);
                    crt2.Series[1].Points.AddY(3);
                    crt2.Series[2].Points.AddY(2);

                    crt2.Series[0].Points.AddXY("en proceso", 7);
                    crt2.Series[1].Points.AddY(3);
                    crt2.Series[2].Points.AddY(0);

                    crt2.Series[0].Points.AddXY("finalizado", 11);
                    crt2.Series[1].Points.AddY(7);
                    crt2.Series[2].Points.AddY(2);

                    crt2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    crt2.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    crt2.Series[2].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    break;

                case 2:
                    crt2.Text = "Informática";

                    crt2.Series.Add("Jorge");
                    crt2.Series.Add("Victor");
                    crt2.Series.Add("Gonzalo");


                    crt2.Series[0].Points.AddXY("pendientes", 5);
                    crt2.Series[1].Points.AddY(3);
                    crt2.Series[2].Points.AddY(20);

                    crt2.Series[0].Points.AddXY("en proceso", 2);
                    crt2.Series[1].Points.AddY(5);
                    crt2.Series[2].Points.AddY(1);

                    crt2.Series[0].Points.AddXY("finalizado", 5);
                    crt2.Series[1].Points.AddY(10);
                    crt2.Series[2].Points.AddY(0);

                    crt2.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    crt2.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    crt2.Series[2].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn;
                    break;
                default:
                    break;
            }

            formCargado = true;
        }

        private void CargaCrtInformeCrt1(int nivel,int IdEmpresa = 0,int IdProceso = 0,int periodo = 0 ) {
            //TODO: 1   implementar gráfico uno en nivel 1 con todo seleccionado (una empresa todos los procesos)
            //TODO: 2   implementar gráfico uno en nivel 1 con un proceso seleccionado (una empresa un proceso)
            //TODO: 3   validar si la empresa/proceso tiene datos, si no, desabilitar seleccion* (punto a considerar, ver si influye en los otros gráficos
            //TODO: 4   implementar segundo gráfico
            //TODO: 5   mostrar data asociada y permitir guardar en excel

            crt1.DataSource = null;
            crt1.Series.Clear();
            formCargado = false;
            crt1.Series.Add("pendiente");
            crt1.Series.Add("en proceso");
            Reporte.Periodo _periodo;
            switch (periodo)
            {
                case 0:
                    _periodo = Reporte.Periodo.semana;
                    break;
                case 1:
                    _periodo = Reporte.Periodo.mes;
                    break;
                case 2:
                    _periodo = Reporte.Periodo.meses;
                    break;
                default:
                    _periodo = Reporte.Periodo.semana;
                    break;
            }

            List<Reporte.EstadosEnElTiempo> historicos = null ;
            switch (nivel)
            {
                case 0:
                case 1:
                    crt1.Text = nivel == 0 ? "Todas" : Empresa.EmpresaByID(IdEmpresa).RazonSocial;

                    historicos = nivel==0?Reporte.EstadosEnLosTiempos(_periodo):Reporte.EstadosEnLosTiempos(IdEmpresa,_periodo);
                    break;
                case 2:
                    historicos = Reporte.EstadosEnLosTiemposByProceso(IdProceso,_periodo);
                    break;
                default:
                    break;
            }

            foreach (Reporte.EstadosEnElTiempo historico in historicos)
            {
                switch (historico.Estado)
                {
                    case 1:
                        crt1.Series[0].Points.AddXY(historico.Fecha.ToString("dd/MM/yyyy"), historico.Cantidad);
                        break;
                    case 2:
                        crt1.Series[1].Points.AddXY(historico.Fecha.ToString("dd/MM/yyyy"), historico.Cantidad);
                        break;
                    default:
                        break;
                }
            }

            crt1.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            crt1.Series[1].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

            crt1.Series[0].BorderWidth = 3;
            crt1.Series[1].BorderWidth = 3;

            crt1.Series[0].Color = Color.Orange;
            crt1.Series[1].Color = Color.Green;

            if (historicos.Count == 0)
            {
                MessageBox.Show("no hay datos para visualizar");
                dgvInforme.Rows[0].Selected = true;
                dgvInforme_CurrentCellChanged(new object(), new EventArgs());
            }

            formCargado = true;
        }
        #endregion


        #region GESTORES
        private string gestorFiltroEmpresa()
        {
            if (cmbUsu_empresa.SelectedItem != null)
            {
                return $"empresaID = {cmbUsu_empresa.SelectedValue}";
            }
            else
            {
                return null;
            }
        }

        private string gestorFiltroCargo()
        {
            if (cmbUsu_Cargo.SelectedItem != null)
            {
                return $"cargoID = {cmbUsu_Cargo.SelectedValue}";
            }
            else
            {
                return null;
            }
        }

        private string gestorFiltroPerfil()
        {
            if (cmbUsu_perfil.SelectedItem != null)
            {
                return $"perfilID = {cmbUsu_perfil.SelectedValue}";
            }
            else
            {
                return null;
            }
        }

        private string gestorFiltroCorreo()
        {
            if (txtUsu_Correo.Text != null && txtUsu_Correo.Text != string.Empty)
            {
                return $"correo LIKE '%{txtUsu_Correo.Text}%'";
            }
            else
            {
                return null;
            }
        }

        private void AplicarFiltro()
        {
            if (formCargado)
            {
                string[] listaConsulta =
                    new string[] {
                        gestorFiltroEmpresa(),
                        gestorFiltroPerfil(),
                        gestorFiltroCargo(),
                        gestorFiltroCorreo()
                    };
                listaConsulta = listaConsulta.Where(lc => lc != null).ToArray();
                string consulta = string.Join(" AND ", listaConsulta);

                dttUsuarios.DefaultView.RowFilter = consulta;
            }
        }

        /// <summary>
        /// Retorna la empresa que haya sido seleccionada cuando la posición del reporte es empresa.
        /// Nulo si está la selección en TODO
        /// </summary>
        /// <returns>Empresa seleccionada</returns>
        /// <example>{idEmpresa=1,razonSocial="PROCESS S.A."}</example>
        private Empresa SeleccionDGVInformesEmpresa()
        {
            try
            {
                int indexSelected = dgvInforme.SelectedRows[0].Index;
                string nombreEmpresa = dttInformeEmpresas.Rows[indexSelected][1].ToString();
                int idEmpresa = 0;
                int.TryParse(dttInformeEmpresas.Rows[indexSelected][0].ToString(), out idEmpresa);

                if (idEmpresa != 0)
                {
                    return new Empresa(idEmpresa, nombreEmpresa);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private Proceso SeleccionDGVInformesProceso()
        {
            try
            {
                int indexSelected = dgvInforme.SelectedRows[0].Index;
                string nombreProceso = dttInformeProcesos.Rows[indexSelected][1].ToString();
                int idProceso = 0;
                int.TryParse(dttInformeProcesos.Rows[indexSelected][0].ToString(), out idProceso);

                if (idProceso != 0)
                {
                    return Proceso.buscarPorID(idProceso);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion


        #region ACCIONES DE CONTROLADOR
        private void btnLogin_Click(object sender, EventArgs e)
        {
            CredencialesAcceso();
        }

        private void cmbUsu_empresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            AplicarFiltro();
        }

        private void cmbUsu_perfil_SelectedIndexChanged(object sender, EventArgs e)
        {
            AplicarFiltro();
        }

        private void cmbUsu_Cargo_SelectedIndexChanged(object sender, EventArgs e)
        {
            AplicarFiltro();
        }

        private void txtUsu_Correo_TextChanged(object sender, EventArgs e)
        {
            AplicarFiltro();
        }

        private void btnUsu_Nuevo_Click(object sender, EventArgs e)
        {
            frmNuevos.FrmAddUsuario frmAddUsu = new frmNuevos.FrmAddUsuario();
            frmAddUsu.ShowDialog();
            CargaFormulario();
        }

        private void btnUsu_Editar_Click(object sender, EventArgs e)
        {
            try
            {
                var userSelected = dgvUsuario.SelectedRows[0];
                var usuario = dttUsuarios.DefaultView.Table.Rows[userSelected.Index];


                var idUsuario = int.Parse(usuario[0].ToString());
                var nombreUsuario = usuario[1].ToString();
                var nombre = usuario[2].ToString();
                var apellido = usuario[3].ToString();
                var correo = usuario[4].ToString();
                var perfilID = int.Parse(usuario[5].ToString());
                var cargoID = int.Parse(usuario[7].ToString());
                var empresaID = int.Parse(usuario[9].ToString());

                Usuario _usuario = new Usuario((int)idUsuario, nombreUsuario.ToString(), nombre, apellido, correo, perfilID, cargoID, empresaID);
                frmEditar.FrmEditUsuario frmEditUsuario = new frmEditar.FrmEditUsuario(_usuario);
                frmEditUsuario.ShowDialog(this);

                CargaFormulario();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnUsu_Eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var userSelected = dgvUsuario.SelectedRows[0];
                var usuario = dttUsuarios.DefaultView.Table.Rows[userSelected.Index];


                var idUsuario = int.Parse(usuario[0].ToString());
                var nombreUsuario = usuario[1].ToString();
                var nombre = usuario[2].ToString();
                var apellido = usuario[3].ToString();
                var correo = usuario[4].ToString();
                var perfilID = int.Parse(usuario[5].ToString());
                var cargoID = int.Parse(usuario[7].ToString());
                var empresaID = int.Parse(usuario[9].ToString());

                Usuario _usuario = new Usuario((int)idUsuario, nombreUsuario.ToString(), nombre, apellido, correo, perfilID, cargoID, empresaID);
                frmEliminar.FrmUsuarioEliminar frmUsuarioEliminar = new frmEliminar.FrmUsuarioEliminar(_usuario);
                frmUsuarioEliminar.ShowDialog(this);
                CargaFormulario();

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEmpresa_Click(object sender, EventArgs e)
        {
            FrmEmpresa frmEmpresa = new FrmEmpresa();
            frmEmpresa.ShowDialog();
            CargaFormulario();
        }

        private void btnCargo_Click(object sender, EventArgs e)
        {
            FrmCargo frmCargo = new FrmCargo();
            frmCargo.ShowDialog(this);
            CargaFormulario();
        }

        // INFORMES Y REPORTES

        private void dgvInforme_CurrentCellChanged(object sender, EventArgs e)
        {
            //navegación del panel lateral
            if (formCargado && nvlInforme == 0)
            {
                Empresa _empresa = SeleccionDGVInformesEmpresa();
                btnVerProceso.Enabled = _empresa != null ? true : false;
                lblInfTitulo.Text = SeleccionDGVInformesEmpresa() != null? SeleccionDGVInformesEmpresa().RazonSocial:"TODOS";
            }
            //actualización de gráficos
            CambioEstadoReporte();
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            if (nvlInforme == 1)
            {
                CargaDGVInforme(0);
                nvlInforme = 0;
                btnVolver.Enabled = false;
                lblInfTitulo.Text = "TODOS";
            }
        }

        private void cmbRangoCrt1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (formCargado)
            {
                CambioEstadoReporte();
            }
        }

        private void btnVerProceso_Click(object sender, EventArgs e)
        {
            nvlInforme = 1;
            CargaDGVInforme(1, (int)SeleccionDGVInformesEmpresa().IdEmpresa);
            btnVolver.Enabled = true;
            btnVerProceso.Enabled = false;
        }
        #endregion


    }
}
