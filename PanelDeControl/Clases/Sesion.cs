﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PanelDeControl.Clases
{
    class Sesion
    {
        int idUsuario;
        string nombreUsuario;
        string token;

        public Sesion(int idUsuario, string nombreUsuario, string token)
        {
            this.IdUsuario = idUsuario;
            this.NombreUsuario = nombreUsuario;
            this.Token = token;
        }

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string NombreUsuario { get => nombreUsuario; set => nombreUsuario = value; }
        public string Token { get => token; set => token = value; }
    }
}
