﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PanelDeControl.Clases
{
    class Auxiliar
    {
        private static WebRequest request;
        //private static string archivoLog = @"C:\CenturyPosLog\log.txt";
        public enum Metodo { GET, POST }

        public static string Peticion(string url, Metodo metodo, string datos = null, string token = null)
        {
            //CrearCarpetaLog();
            string ret = "";

            try
            {
                switch (metodo)
                {
                    case Metodo.POST:

                        request = WebRequest.Create(url);
                        request.PreAuthenticate = true;
                        request.Headers.Add("x-api-key", token);
                        request.ContentType = "application/json; charset=utf-8";
                        request.Method = "POST";
                        byte[] byteArray = Encoding.UTF8.GetBytes(datos);
                        request.ContentLength = byteArray.Length;
                        Stream dataStream = request.GetRequestStream();
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                        WebResponse response = request.GetResponse();
                        using (dataStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(dataStream);
                            ret = reader.ReadToEnd();
                        }
                        response.Close();
                        break;

                    case Metodo.GET:
                        request = WebRequest.Create(url);
                        request.ContentType = "application/json; charset=utf-8";
                        request.Method = "GET";
                        WebResponse response2 = request.GetResponse();
                        using (dataStream = response2.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(dataStream);
                            ret = reader.ReadToEnd();
                        }
                        response2.Close();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //AgregarLog("|003|EX|PeticionPost|" + ex.Message + "|");
                ret = "{ error : " + ex.Message + "}";
            }
            return ret;
        }
    }
}
