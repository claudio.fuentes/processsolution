﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;
using ProcessApi.Auxiliar;
using PanelDeControl.Clases;

namespace PanelDeControl.frmNuevos
{
    public partial class FrmAddEmpresa : Form
    {
        public FrmAddEmpresa()
        {
            InitializeComponent();
        }

        private bool ValidarCampos()
        {
            return txtRazonSocial.Text.Trim(' ') != "";
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {

                if (ValidarCampos())
                {
                    string rs = txtRazonSocial.Text.Trim(' ');
                    if (Empresa.listaEmpresas().Exists(emp => emp.RazonSocial == rs))
                    {
                        MessageBox.Show($"Ya existe una empresa llamada {rs}", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (Empresa.AgregarEmpresa(new Empresa(null, rs)))
                    {
                        MessageBox.Show($"La empresa {rs} ha sido agregada exitosamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("No ha sido posible agregar la empresa", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("El Campo Razón Social no puede estar vacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
            txtRazonSocial.Focus();
        }

        private void txtRazonSocial_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAgregar_Click(sender, new EventArgs());
            }
        }
    }
}
