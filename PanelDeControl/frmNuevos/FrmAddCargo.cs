﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;

namespace PanelDeControl.frmNuevos
{
    public partial class FrmAddCargo : Form
    {
        public FrmAddCargo()
        {
            InitializeComponent();
        }

        private bool ValidarCampos()
        {
            return txtCargo.Text.Trim(' ') != "";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValidarCampos())
                {
                    string cg = txtCargo.Text.Trim(' ');
                    if (Cargo.listaCargos().Exists(carg => carg.Descripcion == cg))
                    {
                        MessageBox.Show($"Ya existe un cargo llamado {cg}", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (Cargo.AgregarCargo(new Cargo(null, cg)))
                    {
                        MessageBox.Show($"el cargo {cg} ha sido agregada exitosamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show("No ha sido posible agregar el cargo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("El Campo Cargo no puede estar vacio", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Close();
            }
            txtCargo.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
