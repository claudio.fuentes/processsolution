﻿using ProcessApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace PanelDeControl.frmNuevos
{
    public partial class FrmAddUsuario : Form
    {
        public FrmAddUsuario()
        {
            InitializeComponent();
            CargaCmb();
            txtCorreo.Focus();
        }



        #region ACCIONES PRINCIPALES
        /// <summary>
        /// carga con datos todos los combobox
        /// </summary>
        void CargaCmb()
        {
            try
            {
                cmbPerfil.DataSource = Perfil.listaPerfiles();
                cmbPerfil.DisplayMember = "Descripcion";
                cmbPerfil.ValueMember = "IdPerfil";
                cmbPerfil.SelectedItem = null;

                cmbEmpresa.DataSource = Empresa.listaEmpresas();
                cmbEmpresa.DisplayMember = "RazonSocial";
                cmbEmpresa.ValueMember = "IdEmpresa";
                cmbEmpresa.SelectedItem = null;

                cmbCargo.DataSource = Cargo.listaCargos();
                cmbCargo.DisplayMember = "Descripcion";
                cmbCargo.ValueMember = "IdCargo";
                cmbCargo.SelectedItem = null;

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// limpia el formulario dejando sus valores por defecto
        /// </summary>
        void Limpiar()
        {
            txtCorreo.Text = string.Empty;
            txtUsuario.Text = string.Empty;
            txtContrasenna.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtApellido.Text = string.Empty;
            cmbPerfil.SelectedItem = null;
            cmbEmpresa.SelectedItem = null;
            cmbCargo.SelectedItem = null;
        }

        bool nuevoUsuario()
        {
            try
            {
                string contrasenna = ProcessApi.Auxiliar.Panacea.Md5(txtContrasenna.Text);
                string nombreUsuario = txtUsuario.Text;
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string correo = txtCorreo.Text;
                int
                    perfilID = (int)cmbPerfil.SelectedValue,
                    cargoID = (int)cmbCargo.SelectedValue,
                    empresaID = (int)cmbEmpresa.SelectedValue;

                return Usuario.AgregarUsuario(new Usuario(nombreUsuario, nombre, apellido, correo, perfilID, cargoID, empresaID), contrasenna);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region FUNCIONES DE CONTROLES
        private void txtCorreo_TextChanged(object sender, EventArgs e)
        {
            if (txtCorreo.Text.Length <= txtUsuario.MaxLength)
            {
                txtUsuario.Text = txtCorreo.Text;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //TODO: corregir problema en el valor dado a la clave primaria.
            if (ValidarCampos())
            {
                if (nuevoUsuario())
                    Close();
            }
        }
        #endregion

        #region VALIDADORES
        /// <summary>
        /// valida que los campos estén completos según requerimientos.
        /// visualiza y actualiza según el caso, las etiquetas de advertencia para el usuario.
        /// </summary>
        /// <returns>estado de los campos</returns>
        bool ValidarCampos()
        {
            bool estado = true;
            if (txtCorreo.Text != string.Empty)
            {
                if (!validacionCorreo())
                {
                    lblCorreo.Visible = true;
                    estado = false;
                }
                else
                {
                    lblCorreo.Visible = false;
                }
            }

            if (txtUsuario.Text != string.Empty)
            {
                lblUsuario.Visible = false;
            }
            else
            {
                lblUsuario.Visible = true;
                estado = false;
            }

            if (txtContrasenna.Text != string.Empty)
            {
                if (ValidarContrasenna())
                {
                    lblContrasenna.Visible = false;
                }
                else
                {
                    lblContrasenna.Text = "contraseña muy corta";
                    lblContrasenna.Visible = true;
                    estado = false;
                }

            }
            else
            {
                lblContrasenna.Text = "*campo requerido";
                lblContrasenna.Visible = true;
                estado = false;
            }

            if (txtNombre.Text != string.Empty)
            {
                lblNombre.Visible = false;
            }
            else
            {
                lblNombre.Visible = true;
                estado = false;
            }

            if (txtApellido.Text != string.Empty)
            {
                lblApellido.Visible = false;
            }
            else
            {
                lblApellido.Visible = true;
                estado = false;
            }
            if (cmbPerfil.SelectedItem != null)
            {
                lblPerfil.Visible = false;
            }
            else
            {
                lblPerfil.Visible = true;
                estado = false;
            }
            if (cmbEmpresa.SelectedItem != null)
            {
                lblEmpresa.Visible = false;
            }
            else
            {
                lblEmpresa.Visible = true;
                estado = false;
            }
            if (cmbCargo.SelectedItem != null)
            {
                lblCargo.Visible = false;
            }
            else
            {
                lblCargo.Visible = true;
                estado = false;
            }

            return estado;
        }
        private Boolean validacionCorreo()
        {
            String email = txtCorreo.Text;
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        private bool ValidarContrasenna()
        {
            string pass = txtContrasenna.Text;

            return pass.Length >= 5;
        }
        #endregion


    }
}
