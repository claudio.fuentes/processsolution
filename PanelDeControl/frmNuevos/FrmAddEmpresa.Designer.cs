﻿namespace PanelDeControl.frmNuevos
{
    partial class FrmAddEmpresa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp0All = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Titulo = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Formulario = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Botonera = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRazonSocial = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.tlp0All.SuspendLayout();
            this.tlp1Titulo.SuspendLayout();
            this.tlp1Formulario.SuspendLayout();
            this.tlp1Botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp0All
            // 
            this.tlp0All.ColumnCount = 1;
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlp0All.Controls.Add(this.tlp1Titulo, 0, 0);
            this.tlp0All.Controls.Add(this.tlp1Formulario, 0, 1);
            this.tlp0All.Controls.Add(this.tlp1Botonera, 0, 2);
            this.tlp0All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp0All.Location = new System.Drawing.Point(0, 0);
            this.tlp0All.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tlp0All.Name = "tlp0All";
            this.tlp0All.RowCount = 3;
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlp0All.Size = new System.Drawing.Size(381, 202);
            this.tlp0All.TabIndex = 0;
            // 
            // tlp1Titulo
            // 
            this.tlp1Titulo.ColumnCount = 1;
            this.tlp1Titulo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Titulo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlp1Titulo.Controls.Add(this.label1, 0, 0);
            this.tlp1Titulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Titulo.Location = new System.Drawing.Point(5, 4);
            this.tlp1Titulo.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tlp1Titulo.Name = "tlp1Titulo";
            this.tlp1Titulo.RowCount = 1;
            this.tlp1Titulo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Titulo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tlp1Titulo.Size = new System.Drawing.Size(371, 32);
            this.tlp1Titulo.TabIndex = 0;
            // 
            // tlp1Formulario
            // 
            this.tlp1Formulario.ColumnCount = 1;
            this.tlp1Formulario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Formulario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlp1Formulario.Controls.Add(this.txtRazonSocial, 0, 0);
            this.tlp1Formulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Formulario.Location = new System.Drawing.Point(5, 44);
            this.tlp1Formulario.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tlp1Formulario.Name = "tlp1Formulario";
            this.tlp1Formulario.RowCount = 1;
            this.tlp1Formulario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Formulario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tlp1Formulario.Size = new System.Drawing.Size(371, 93);
            this.tlp1Formulario.TabIndex = 1;
            // 
            // tlp1Botonera
            // 
            this.tlp1Botonera.ColumnCount = 2;
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.Controls.Add(this.btnAgregar, 1, 0);
            this.tlp1Botonera.Controls.Add(this.btnCancelar, 0, 0);
            this.tlp1Botonera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Botonera.Location = new System.Drawing.Point(5, 145);
            this.tlp1Botonera.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tlp1Botonera.Name = "tlp1Botonera";
            this.tlp1Botonera.RowCount = 1;
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 79F));
            this.tlp1Botonera.Size = new System.Drawing.Size(371, 53);
            this.tlp1Botonera.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(5, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(361, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "nueva razón social";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // txtRazonSocial
            // 
            this.txtRazonSocial.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRazonSocial.Location = new System.Drawing.Point(5, 33);
            this.txtRazonSocial.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.txtRazonSocial.Name = "txtRazonSocial";
            this.txtRazonSocial.Size = new System.Drawing.Size(361, 27);
            this.txtRazonSocial.TabIndex = 0;
            this.txtRazonSocial.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRazonSocial_KeyUp);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancelar.Location = new System.Drawing.Point(19, 5);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(147, 42);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAgregar.Location = new System.Drawing.Point(204, 5);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(147, 42);
            this.btnAgregar.TabIndex = 1;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // FrmAddEmpresa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(381, 202);
            this.Controls.Add(this.tlp0All);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmAddEmpresa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmAddEmpresa";
            this.tlp0All.ResumeLayout(false);
            this.tlp1Titulo.ResumeLayout(false);
            this.tlp1Titulo.PerformLayout();
            this.tlp1Formulario.ResumeLayout(false);
            this.tlp1Formulario.PerformLayout();
            this.tlp1Botonera.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp0All;
        private System.Windows.Forms.TableLayoutPanel tlp1Titulo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tlp1Formulario;
        private System.Windows.Forms.TextBox txtRazonSocial;
        private System.Windows.Forms.TableLayoutPanel tlp1Botonera;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnCancelar;
    }
}