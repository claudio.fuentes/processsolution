﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PanelDeControl.Clases;
using Newtonsoft.Json;
using ProcessApi.Auxiliar;

namespace PanelDeControl
{
    public partial class frmLogin : Form
    {
        
        public frmLogin()
        {
            InitializeComponent();
            

            txtUsuario.Text = "admin@process.cl";
            txtContrasenna.Text = "123";

            //tbxUsuario.Text = Properties.Settings.Default.nombreUsuario;
            //tbxContrasenna.Text = Properties.Settings.Default.contrasenna;
        }
        
        private void btnAcceder_Click(object sender, EventArgs e)
        {
            string usuario, contrasenna;
            usuario = txtUsuario.Text;
            contrasenna = txtContrasenna.Text;
            //contrasenna = Panacea.Md5(contrasenna);

            dynamic dataSending = new { usuario, contrasenna };
            //string datos = "{" + String.Format("\"usuario\":\"{0}\",\"contrasenna\":\"{1}\"", usuario, contrasenna) + "}";
            dynamic jsonObj = JsonConvert.DeserializeObject(Auxiliar.Peticion("http://localhost:63826/api/Usuario/Validar", Auxiliar.Metodo.POST, JsonConvert.SerializeObject(dataSending)));

            var codigo = jsonObj["codigo"].ToString();


            if (int.Parse(codigo) == 0)
            {

                if (cbxRecuerdame.Checked)
                {
                    Properties.Settings.Default.nombreUsuario = usuario;
                    Properties.Settings.Default.contrasenna = contrasenna;
                    Properties.Settings.Default.recuerdame = true;
                }
                else
                {
                    Properties.Settings.Default.recuerdame = false;
                }
                string token = jsonObj["data"]["token"];

                FrmMaestra padre = (FrmMaestra)this.Owner;
                padre.Sesion = new Sesion(1, usuario, token);

                this.Close();
            }
            else
            {
                MessageBox.Show("Credenciales inválidas");
            }
        }
    }
}
