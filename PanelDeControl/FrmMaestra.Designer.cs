﻿namespace PanelDeControl
{
    partial class FrmMaestra
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMaestra));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tlp0All = new System.Windows.Forms.TableLayoutPanel();
            this.tlpI1Informacion = new System.Windows.Forms.TableLayoutPanel();
            this.tlp2InfoUsuario = new System.Windows.Forms.TableLayoutPanel();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.pbxLogo = new System.Windows.Forms.PictureBox();
            this.tclPrincipal = new System.Windows.Forms.TabControl();
            this.tpgUsuario = new System.Windows.Forms.TabPage();
            this.tlp2panelUsuario = new System.Windows.Forms.TableLayoutPanel();
            this.dgvUsuario = new System.Windows.Forms.DataGridView();
            this.tlp3CabeceraUsuario = new System.Windows.Forms.TableLayoutPanel();
            this.tlp4Filtros = new System.Windows.Forms.TableLayoutPanel();
            this.cmbUsu_Cargo = new System.Windows.Forms.ComboBox();
            this.cmbUsu_perfil = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsu_Correo = new System.Windows.Forms.TextBox();
            this.cmbUsu_empresa = new System.Windows.Forms.ComboBox();
            this.tlp4BotoneraUsuario = new System.Windows.Forms.TableLayoutPanel();
            this.btnUsu_Nuevo = new System.Windows.Forms.Button();
            this.btnUsu_Editar = new System.Windows.Forms.Button();
            this.btnUsu_Eliminar = new System.Windows.Forms.Button();
            this.tlp4Gestiones = new System.Windows.Forms.TableLayoutPanel();
            this.btnCargo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEmpresa = new System.Windows.Forms.Button();
            this.tpg2Reporteria = new System.Windows.Forms.TabPage();
            this.tlp2Reporteria = new System.Windows.Forms.TableLayoutPanel();
            this.tlp3Arbol = new System.Windows.Forms.TableLayoutPanel();
            this.tlp4Titulo = new System.Windows.Forms.TableLayoutPanel();
            this.lblInfTitulo = new System.Windows.Forms.Label();
            this.tlp4Formulario = new System.Windows.Forms.TableLayoutPanel();
            this.dgvInforme = new System.Windows.Forms.DataGridView();
            this.tlp4Botonera = new System.Windows.Forms.TableLayoutPanel();
            this.btnVerProceso = new System.Windows.Forms.Button();
            this.btnVolver = new System.Windows.Forms.Button();
            this.tlp3Graficos = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.crt2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tlp4Grafico1 = new System.Windows.Forms.TableLayoutPanel();
            this.tlp5Gr1Grafico = new System.Windows.Forms.TableLayoutPanel();
            this.crt1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tlp5Gr1Info = new System.Windows.Forms.TableLayoutPanel();
            this.btnRepoRefrescar = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbRangoCrt1 = new System.Windows.Forms.ComboBox();
            this.tlp0All.SuspendLayout();
            this.tlpI1Informacion.SuspendLayout();
            this.tlp2InfoUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).BeginInit();
            this.tclPrincipal.SuspendLayout();
            this.tpgUsuario.SuspendLayout();
            this.tlp2panelUsuario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuario)).BeginInit();
            this.tlp3CabeceraUsuario.SuspendLayout();
            this.tlp4Filtros.SuspendLayout();
            this.tlp4BotoneraUsuario.SuspendLayout();
            this.tlp4Gestiones.SuspendLayout();
            this.tpg2Reporteria.SuspendLayout();
            this.tlp2Reporteria.SuspendLayout();
            this.tlp3Arbol.SuspendLayout();
            this.tlp4Titulo.SuspendLayout();
            this.tlp4Formulario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInforme)).BeginInit();
            this.tlp4Botonera.SuspendLayout();
            this.tlp3Graficos.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crt2)).BeginInit();
            this.tlp4Grafico1.SuspendLayout();
            this.tlp5Gr1Grafico.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crt1)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp0All
            // 
            this.tlp0All.ColumnCount = 1;
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp0All.Controls.Add(this.tlpI1Informacion, 0, 0);
            this.tlp0All.Controls.Add(this.tclPrincipal, 0, 1);
            this.tlp0All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp0All.Location = new System.Drawing.Point(0, 0);
            this.tlp0All.Name = "tlp0All";
            this.tlp0All.RowCount = 2;
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92F));
            this.tlp0All.Size = new System.Drawing.Size(1177, 717);
            this.tlp0All.TabIndex = 0;
            // 
            // tlpI1Informacion
            // 
            this.tlpI1Informacion.ColumnCount = 3;
            this.tlpI1Informacion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpI1Informacion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpI1Informacion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpI1Informacion.Controls.Add(this.tlp2InfoUsuario, 2, 0);
            this.tlpI1Informacion.Controls.Add(this.pbxLogo, 0, 0);
            this.tlpI1Informacion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpI1Informacion.Location = new System.Drawing.Point(3, 3);
            this.tlpI1Informacion.Name = "tlpI1Informacion";
            this.tlpI1Informacion.RowCount = 1;
            this.tlpI1Informacion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpI1Informacion.Size = new System.Drawing.Size(1171, 51);
            this.tlpI1Informacion.TabIndex = 0;
            // 
            // tlp2InfoUsuario
            // 
            this.tlp2InfoUsuario.ColumnCount = 2;
            this.tlp2InfoUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.92308F));
            this.tlp2InfoUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.07692F));
            this.tlp2InfoUsuario.Controls.Add(this.lblUsuario, 0, 0);
            this.tlp2InfoUsuario.Controls.Add(this.btnLogin, 1, 0);
            this.tlp2InfoUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp2InfoUsuario.Location = new System.Drawing.Point(588, 3);
            this.tlp2InfoUsuario.Name = "tlp2InfoUsuario";
            this.tlp2InfoUsuario.RowCount = 1;
            this.tlp2InfoUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlp2InfoUsuario.Size = new System.Drawing.Size(580, 45);
            this.tlp2InfoUsuario.TabIndex = 0;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUsuario.Font = new System.Drawing.Font("Verdana", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.Location = new System.Drawing.Point(3, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(440, 45);
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "usuario no logeado";
            this.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnLogin
            // 
            this.btnLogin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLogin.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(449, 6);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(128, 33);
            this.btnLogin.TabIndex = 2;
            this.btnLogin.Text = "iniciar sesión";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // pbxLogo
            // 
            this.pbxLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pbxLogo.Image")));
            this.pbxLogo.Location = new System.Drawing.Point(3, 3);
            this.pbxLogo.Name = "pbxLogo";
            this.pbxLogo.Size = new System.Drawing.Size(111, 45);
            this.pbxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxLogo.TabIndex = 1;
            this.pbxLogo.TabStop = false;
            // 
            // tclPrincipal
            // 
            this.tclPrincipal.Controls.Add(this.tpgUsuario);
            this.tclPrincipal.Controls.Add(this.tpg2Reporteria);
            this.tclPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tclPrincipal.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tclPrincipal.Location = new System.Drawing.Point(3, 60);
            this.tclPrincipal.Name = "tclPrincipal";
            this.tclPrincipal.SelectedIndex = 0;
            this.tclPrincipal.Size = new System.Drawing.Size(1171, 654);
            this.tclPrincipal.TabIndex = 1;
            // 
            // tpgUsuario
            // 
            this.tpgUsuario.Controls.Add(this.tlp2panelUsuario);
            this.tpgUsuario.Location = new System.Drawing.Point(4, 25);
            this.tpgUsuario.Name = "tpgUsuario";
            this.tpgUsuario.Padding = new System.Windows.Forms.Padding(3);
            this.tpgUsuario.Size = new System.Drawing.Size(1163, 625);
            this.tpgUsuario.TabIndex = 0;
            this.tpgUsuario.Text = "Gestiones de Usuario";
            this.tpgUsuario.UseVisualStyleBackColor = true;
            // 
            // tlp2panelUsuario
            // 
            this.tlp2panelUsuario.ColumnCount = 1;
            this.tlp2panelUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp2panelUsuario.Controls.Add(this.dgvUsuario, 0, 1);
            this.tlp2panelUsuario.Controls.Add(this.tlp3CabeceraUsuario, 0, 0);
            this.tlp2panelUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp2panelUsuario.Location = new System.Drawing.Point(3, 3);
            this.tlp2panelUsuario.Name = "tlp2panelUsuario";
            this.tlp2panelUsuario.RowCount = 2;
            this.tlp2panelUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlp2panelUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tlp2panelUsuario.Size = new System.Drawing.Size(1157, 619);
            this.tlp2panelUsuario.TabIndex = 1;
            // 
            // dgvUsuario
            // 
            this.dgvUsuario.AllowUserToAddRows = false;
            this.dgvUsuario.AllowUserToDeleteRows = false;
            this.dgvUsuario.AllowUserToOrderColumns = true;
            this.dgvUsuario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUsuario.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvUsuario.Location = new System.Drawing.Point(3, 188);
            this.dgvUsuario.MultiSelect = false;
            this.dgvUsuario.Name = "dgvUsuario";
            this.dgvUsuario.ReadOnly = true;
            this.dgvUsuario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsuario.Size = new System.Drawing.Size(1151, 428);
            this.dgvUsuario.TabIndex = 0;
            // 
            // tlp3CabeceraUsuario
            // 
            this.tlp3CabeceraUsuario.ColumnCount = 3;
            this.tlp3CabeceraUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp3CabeceraUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp3CabeceraUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp3CabeceraUsuario.Controls.Add(this.tlp4Filtros, 0, 0);
            this.tlp3CabeceraUsuario.Controls.Add(this.tlp4BotoneraUsuario, 1, 0);
            this.tlp3CabeceraUsuario.Controls.Add(this.tlp4Gestiones, 2, 0);
            this.tlp3CabeceraUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp3CabeceraUsuario.Location = new System.Drawing.Point(3, 3);
            this.tlp3CabeceraUsuario.Name = "tlp3CabeceraUsuario";
            this.tlp3CabeceraUsuario.RowCount = 1;
            this.tlp3CabeceraUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp3CabeceraUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tlp3CabeceraUsuario.Size = new System.Drawing.Size(1151, 179);
            this.tlp3CabeceraUsuario.TabIndex = 0;
            // 
            // tlp4Filtros
            // 
            this.tlp4Filtros.ColumnCount = 2;
            this.tlp4Filtros.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp4Filtros.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp4Filtros.Controls.Add(this.cmbUsu_Cargo, 1, 3);
            this.tlp4Filtros.Controls.Add(this.cmbUsu_perfil, 1, 2);
            this.tlp4Filtros.Controls.Add(this.label1, 0, 0);
            this.tlp4Filtros.Controls.Add(this.label2, 0, 1);
            this.tlp4Filtros.Controls.Add(this.label3, 0, 2);
            this.tlp4Filtros.Controls.Add(this.label4, 0, 3);
            this.tlp4Filtros.Controls.Add(this.txtUsu_Correo, 1, 0);
            this.tlp4Filtros.Controls.Add(this.cmbUsu_empresa, 1, 1);
            this.tlp4Filtros.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Filtros.Location = new System.Drawing.Point(3, 3);
            this.tlp4Filtros.Name = "tlp4Filtros";
            this.tlp4Filtros.RowCount = 4;
            this.tlp4Filtros.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp4Filtros.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp4Filtros.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp4Filtros.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tlp4Filtros.Size = new System.Drawing.Size(569, 173);
            this.tlp4Filtros.TabIndex = 0;
            // 
            // cmbUsu_Cargo
            // 
            this.cmbUsu_Cargo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUsu_Cargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsu_Cargo.FormattingEnabled = true;
            this.cmbUsu_Cargo.Location = new System.Drawing.Point(287, 132);
            this.cmbUsu_Cargo.Name = "cmbUsu_Cargo";
            this.cmbUsu_Cargo.Size = new System.Drawing.Size(279, 28);
            this.cmbUsu_Cargo.TabIndex = 7;
            this.cmbUsu_Cargo.SelectedIndexChanged += new System.EventHandler(this.cmbUsu_Cargo_SelectedIndexChanged);
            // 
            // cmbUsu_perfil
            // 
            this.cmbUsu_perfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUsu_perfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsu_perfil.FormattingEnabled = true;
            this.cmbUsu_perfil.Location = new System.Drawing.Point(287, 89);
            this.cmbUsu_perfil.Name = "cmbUsu_perfil";
            this.cmbUsu_perfil.Size = new System.Drawing.Size(279, 28);
            this.cmbUsu_perfil.TabIndex = 6;
            this.cmbUsu_perfil.SelectedIndexChanged += new System.EventHandler(this.cmbUsu_perfil_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(66, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 43);
            this.label1.TabIndex = 0;
            this.label1.Text = "Correo Usuario";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(95, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 43);
            this.label2.TabIndex = 1;
            this.label2.Text = "Empresa";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(113, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 43);
            this.label3.TabIndex = 2;
            this.label3.Text = "Perfil";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(109, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 44);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cargo";
            // 
            // txtUsu_Correo
            // 
            this.txtUsu_Correo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtUsu_Correo.Location = new System.Drawing.Point(287, 3);
            this.txtUsu_Correo.Name = "txtUsu_Correo";
            this.txtUsu_Correo.Size = new System.Drawing.Size(279, 23);
            this.txtUsu_Correo.TabIndex = 4;
            this.txtUsu_Correo.TextChanged += new System.EventHandler(this.txtUsu_Correo_TextChanged);
            // 
            // cmbUsu_empresa
            // 
            this.cmbUsu_empresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbUsu_empresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUsu_empresa.FormattingEnabled = true;
            this.cmbUsu_empresa.Location = new System.Drawing.Point(287, 46);
            this.cmbUsu_empresa.Name = "cmbUsu_empresa";
            this.cmbUsu_empresa.Size = new System.Drawing.Size(279, 28);
            this.cmbUsu_empresa.TabIndex = 5;
            this.cmbUsu_empresa.SelectedIndexChanged += new System.EventHandler(this.cmbUsu_empresa_SelectedIndexChanged);
            // 
            // tlp4BotoneraUsuario
            // 
            this.tlp4BotoneraUsuario.ColumnCount = 1;
            this.tlp4BotoneraUsuario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp4BotoneraUsuario.Controls.Add(this.btnUsu_Nuevo, 0, 0);
            this.tlp4BotoneraUsuario.Controls.Add(this.btnUsu_Editar, 0, 1);
            this.tlp4BotoneraUsuario.Controls.Add(this.btnUsu_Eliminar, 0, 2);
            this.tlp4BotoneraUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4BotoneraUsuario.Location = new System.Drawing.Point(578, 3);
            this.tlp4BotoneraUsuario.Name = "tlp4BotoneraUsuario";
            this.tlp4BotoneraUsuario.RowCount = 3;
            this.tlp4BotoneraUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp4BotoneraUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp4BotoneraUsuario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp4BotoneraUsuario.Size = new System.Drawing.Size(281, 173);
            this.tlp4BotoneraUsuario.TabIndex = 1;
            // 
            // btnUsu_Nuevo
            // 
            this.btnUsu_Nuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUsu_Nuevo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUsu_Nuevo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsu_Nuevo.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsu_Nuevo.Location = new System.Drawing.Point(3, 3);
            this.btnUsu_Nuevo.Name = "btnUsu_Nuevo";
            this.btnUsu_Nuevo.Size = new System.Drawing.Size(275, 51);
            this.btnUsu_Nuevo.TabIndex = 2;
            this.btnUsu_Nuevo.Text = "Nuevo";
            this.btnUsu_Nuevo.UseVisualStyleBackColor = false;
            this.btnUsu_Nuevo.Click += new System.EventHandler(this.btnUsu_Nuevo_Click);
            // 
            // btnUsu_Editar
            // 
            this.btnUsu_Editar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUsu_Editar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUsu_Editar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsu_Editar.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsu_Editar.Location = new System.Drawing.Point(3, 60);
            this.btnUsu_Editar.Name = "btnUsu_Editar";
            this.btnUsu_Editar.Size = new System.Drawing.Size(275, 51);
            this.btnUsu_Editar.TabIndex = 0;
            this.btnUsu_Editar.Text = "Editar";
            this.btnUsu_Editar.UseVisualStyleBackColor = false;
            this.btnUsu_Editar.Click += new System.EventHandler(this.btnUsu_Editar_Click);
            // 
            // btnUsu_Eliminar
            // 
            this.btnUsu_Eliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUsu_Eliminar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnUsu_Eliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsu_Eliminar.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUsu_Eliminar.Location = new System.Drawing.Point(3, 117);
            this.btnUsu_Eliminar.Name = "btnUsu_Eliminar";
            this.btnUsu_Eliminar.Size = new System.Drawing.Size(275, 53);
            this.btnUsu_Eliminar.TabIndex = 1;
            this.btnUsu_Eliminar.Text = "Eliminar";
            this.btnUsu_Eliminar.UseVisualStyleBackColor = false;
            this.btnUsu_Eliminar.Click += new System.EventHandler(this.btnUsu_Eliminar_Click);
            // 
            // tlp4Gestiones
            // 
            this.tlp4Gestiones.ColumnCount = 1;
            this.tlp4Gestiones.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp4Gestiones.Controls.Add(this.btnCargo, 0, 2);
            this.tlp4Gestiones.Controls.Add(this.label5, 0, 0);
            this.tlp4Gestiones.Controls.Add(this.btnEmpresa, 0, 1);
            this.tlp4Gestiones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Gestiones.Location = new System.Drawing.Point(865, 3);
            this.tlp4Gestiones.Name = "tlp4Gestiones";
            this.tlp4Gestiones.RowCount = 3;
            this.tlp4Gestiones.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlp4Gestiones.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlp4Gestiones.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlp4Gestiones.Size = new System.Drawing.Size(283, 173);
            this.tlp4Gestiones.TabIndex = 2;
            // 
            // btnCargo
            // 
            this.btnCargo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCargo.Location = new System.Drawing.Point(60, 126);
            this.btnCargo.Name = "btnCargo";
            this.btnCargo.Size = new System.Drawing.Size(162, 32);
            this.btnCargo.TabIndex = 2;
            this.btnCargo.Text = "Cargos";
            this.btnCargo.UseVisualStyleBackColor = true;
            this.btnCargo.Click += new System.EventHandler(this.btnCargo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(277, 51);
            this.label5.TabIndex = 0;
            this.label5.Text = "Gestores";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnEmpresa
            // 
            this.btnEmpresa.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEmpresa.Location = new System.Drawing.Point(60, 65);
            this.btnEmpresa.Name = "btnEmpresa";
            this.btnEmpresa.Size = new System.Drawing.Size(162, 32);
            this.btnEmpresa.TabIndex = 1;
            this.btnEmpresa.Text = "Empresas";
            this.btnEmpresa.UseVisualStyleBackColor = true;
            this.btnEmpresa.Click += new System.EventHandler(this.btnEmpresa_Click);
            // 
            // tpg2Reporteria
            // 
            this.tpg2Reporteria.Controls.Add(this.tlp2Reporteria);
            this.tpg2Reporteria.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpg2Reporteria.Location = new System.Drawing.Point(4, 25);
            this.tpg2Reporteria.Name = "tpg2Reporteria";
            this.tpg2Reporteria.Padding = new System.Windows.Forms.Padding(3);
            this.tpg2Reporteria.Size = new System.Drawing.Size(1163, 625);
            this.tpg2Reporteria.TabIndex = 1;
            this.tpg2Reporteria.Text = "Dashboard";
            this.tpg2Reporteria.UseVisualStyleBackColor = true;
            // 
            // tlp2Reporteria
            // 
            this.tlp2Reporteria.ColumnCount = 2;
            this.tlp2Reporteria.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp2Reporteria.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlp2Reporteria.Controls.Add(this.tlp3Arbol, 0, 0);
            this.tlp2Reporteria.Controls.Add(this.tlp3Graficos, 1, 0);
            this.tlp2Reporteria.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp2Reporteria.Location = new System.Drawing.Point(3, 3);
            this.tlp2Reporteria.Name = "tlp2Reporteria";
            this.tlp2Reporteria.RowCount = 1;
            this.tlp2Reporteria.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp2Reporteria.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 619F));
            this.tlp2Reporteria.Size = new System.Drawing.Size(1157, 619);
            this.tlp2Reporteria.TabIndex = 0;
            // 
            // tlp3Arbol
            // 
            this.tlp3Arbol.ColumnCount = 1;
            this.tlp3Arbol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp3Arbol.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp3Arbol.Controls.Add(this.tlp4Titulo, 0, 0);
            this.tlp3Arbol.Controls.Add(this.tlp4Formulario, 0, 1);
            this.tlp3Arbol.Controls.Add(this.tlp4Botonera, 0, 2);
            this.tlp3Arbol.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp3Arbol.Location = new System.Drawing.Point(3, 3);
            this.tlp3Arbol.Name = "tlp3Arbol";
            this.tlp3Arbol.RowCount = 3;
            this.tlp3Arbol.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp3Arbol.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tlp3Arbol.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp3Arbol.Size = new System.Drawing.Size(225, 613);
            this.tlp3Arbol.TabIndex = 0;
            // 
            // tlp4Titulo
            // 
            this.tlp4Titulo.ColumnCount = 1;
            this.tlp4Titulo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp4Titulo.Controls.Add(this.btnRepoRefrescar, 0, 1);
            this.tlp4Titulo.Controls.Add(this.lblInfTitulo, 0, 0);
            this.tlp4Titulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Titulo.Location = new System.Drawing.Point(3, 3);
            this.tlp4Titulo.Name = "tlp4Titulo";
            this.tlp4Titulo.RowCount = 2;
            this.tlp4Titulo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlp4Titulo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlp4Titulo.Size = new System.Drawing.Size(219, 55);
            this.tlp4Titulo.TabIndex = 0;
            // 
            // lblInfTitulo
            // 
            this.lblInfTitulo.AutoSize = true;
            this.lblInfTitulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInfTitulo.Location = new System.Drawing.Point(3, 0);
            this.lblInfTitulo.Name = "lblInfTitulo";
            this.lblInfTitulo.Size = new System.Drawing.Size(213, 22);
            this.lblInfTitulo.TabIndex = 0;
            this.lblInfTitulo.Text = "TODO";
            this.lblInfTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tlp4Formulario
            // 
            this.tlp4Formulario.ColumnCount = 1;
            this.tlp4Formulario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp4Formulario.Controls.Add(this.dgvInforme, 0, 0);
            this.tlp4Formulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Formulario.Location = new System.Drawing.Point(3, 64);
            this.tlp4Formulario.Name = "tlp4Formulario";
            this.tlp4Formulario.RowCount = 1;
            this.tlp4Formulario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp4Formulario.Size = new System.Drawing.Size(219, 423);
            this.tlp4Formulario.TabIndex = 1;
            // 
            // dgvInforme
            // 
            this.dgvInforme.AllowUserToAddRows = false;
            this.dgvInforme.AllowUserToDeleteRows = false;
            this.dgvInforme.AllowUserToResizeColumns = false;
            this.dgvInforme.AllowUserToResizeRows = false;
            this.dgvInforme.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInforme.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInforme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInforme.Location = new System.Drawing.Point(3, 3);
            this.dgvInforme.MultiSelect = false;
            this.dgvInforme.Name = "dgvInforme";
            this.dgvInforme.ReadOnly = true;
            this.dgvInforme.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvInforme.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInforme.Size = new System.Drawing.Size(213, 417);
            this.dgvInforme.TabIndex = 0;
            this.dgvInforme.CurrentCellChanged += new System.EventHandler(this.dgvInforme_CurrentCellChanged);
            // 
            // tlp4Botonera
            // 
            this.tlp4Botonera.ColumnCount = 2;
            this.tlp4Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlp4Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlp4Botonera.Controls.Add(this.btnVerProceso, 1, 0);
            this.tlp4Botonera.Controls.Add(this.btnVolver, 0, 0);
            this.tlp4Botonera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Botonera.Location = new System.Drawing.Point(3, 493);
            this.tlp4Botonera.Name = "tlp4Botonera";
            this.tlp4Botonera.RowCount = 1;
            this.tlp4Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp4Botonera.Size = new System.Drawing.Size(219, 117);
            this.tlp4Botonera.TabIndex = 2;
            // 
            // btnVerProceso
            // 
            this.btnVerProceso.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerProceso.Enabled = false;
            this.btnVerProceso.Location = new System.Drawing.Point(90, 44);
            this.btnVerProceso.Name = "btnVerProceso";
            this.btnVerProceso.Size = new System.Drawing.Size(126, 29);
            this.btnVerProceso.TabIndex = 1;
            this.btnVerProceso.Text = "ver procesos";
            this.btnVerProceso.UseVisualStyleBackColor = true;
            this.btnVerProceso.Click += new System.EventHandler(this.btnVerProceso_Click);
            // 
            // btnVolver
            // 
            this.btnVolver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVolver.Enabled = false;
            this.btnVolver.Location = new System.Drawing.Point(3, 44);
            this.btnVolver.Name = "btnVolver";
            this.btnVolver.Size = new System.Drawing.Size(81, 29);
            this.btnVolver.TabIndex = 0;
            this.btnVolver.Text = "Volver";
            this.btnVolver.UseVisualStyleBackColor = true;
            this.btnVolver.Click += new System.EventHandler(this.btnVolver_Click);
            // 
            // tlp3Graficos
            // 
            this.tlp3Graficos.ColumnCount = 1;
            this.tlp3Graficos.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp3Graficos.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tlp3Graficos.Controls.Add(this.tlp4Grafico1, 0, 0);
            this.tlp3Graficos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp3Graficos.Location = new System.Drawing.Point(234, 3);
            this.tlp3Graficos.Name = "tlp3Graficos";
            this.tlp3Graficos.RowCount = 2;
            this.tlp3Graficos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp3Graficos.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp3Graficos.Size = new System.Drawing.Size(920, 613);
            this.tlp3Graficos.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 309);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 301F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(914, 301);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.crt2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(633, 295);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // crt2
            // 
            chartArea6.Name = "ChartArea1";
            this.crt2.ChartAreas.Add(chartArea6);
            this.crt2.Dock = System.Windows.Forms.DockStyle.Fill;
            legend6.Name = "Legend1";
            this.crt2.Legends.Add(legend6);
            this.crt2.Location = new System.Drawing.Point(3, 3);
            this.crt2.Name = "crt2";
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.crt2.Series.Add(series6);
            this.crt2.Size = new System.Drawing.Size(627, 289);
            this.crt2.TabIndex = 1;
            this.crt2.Text = "crt2";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(642, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(269, 295);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tlp4Grafico1
            // 
            this.tlp4Grafico1.ColumnCount = 2;
            this.tlp4Grafico1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tlp4Grafico1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlp4Grafico1.Controls.Add(this.tlp5Gr1Grafico, 0, 0);
            this.tlp4Grafico1.Controls.Add(this.tlp5Gr1Info, 1, 0);
            this.tlp4Grafico1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp4Grafico1.Location = new System.Drawing.Point(3, 3);
            this.tlp4Grafico1.Name = "tlp4Grafico1";
            this.tlp4Grafico1.RowCount = 1;
            this.tlp4Grafico1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp4Grafico1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tlp4Grafico1.Size = new System.Drawing.Size(914, 300);
            this.tlp4Grafico1.TabIndex = 0;
            // 
            // tlp5Gr1Grafico
            // 
            this.tlp5Gr1Grafico.ColumnCount = 1;
            this.tlp5Gr1Grafico.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp5Gr1Grafico.Controls.Add(this.crt1, 0, 1);
            this.tlp5Gr1Grafico.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tlp5Gr1Grafico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp5Gr1Grafico.Location = new System.Drawing.Point(3, 3);
            this.tlp5Gr1Grafico.Name = "tlp5Gr1Grafico";
            this.tlp5Gr1Grafico.RowCount = 2;
            this.tlp5Gr1Grafico.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp5Gr1Grafico.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlp5Gr1Grafico.Size = new System.Drawing.Size(633, 294);
            this.tlp5Gr1Grafico.TabIndex = 0;
            // 
            // crt1
            // 
            chartArea5.Name = "ChartArea1";
            this.crt1.ChartAreas.Add(chartArea5);
            this.crt1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend5.Name = "Legend1";
            this.crt1.Legends.Add(legend5);
            this.crt1.Location = new System.Drawing.Point(3, 61);
            this.crt1.Name = "crt1";
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.crt1.Series.Add(series5);
            this.crt1.Size = new System.Drawing.Size(627, 230);
            this.crt1.TabIndex = 0;
            this.crt1.Text = "chart1";
            // 
            // tlp5Gr1Info
            // 
            this.tlp5Gr1Info.ColumnCount = 1;
            this.tlp5Gr1Info.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp5Gr1Info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp5Gr1Info.Location = new System.Drawing.Point(642, 3);
            this.tlp5Gr1Info.Name = "tlp5Gr1Info";
            this.tlp5Gr1Info.RowCount = 3;
            this.tlp5Gr1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp5Gr1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp5Gr1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlp5Gr1Info.Size = new System.Drawing.Size(269, 294);
            this.tlp5Gr1Info.TabIndex = 1;
            // 
            // btnRepoRefrescar
            // 
            this.btnRepoRefrescar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRepoRefrescar.Enabled = false;
            this.btnRepoRefrescar.Location = new System.Drawing.Point(3, 25);
            this.btnRepoRefrescar.Name = "btnRepoRefrescar";
            this.btnRepoRefrescar.Size = new System.Drawing.Size(213, 27);
            this.btnRepoRefrescar.TabIndex = 1;
            this.btnRepoRefrescar.Text = "Refrescar";
            this.btnRepoRefrescar.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmbRangoCrt1, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(627, 52);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(307, 52);
            this.label6.TabIndex = 0;
            this.label6.Text = "rango";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbRangoCrt1
            // 
            this.cmbRangoCrt1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cmbRangoCrt1.FormattingEnabled = true;
            this.cmbRangoCrt1.Location = new System.Drawing.Point(316, 13);
            this.cmbRangoCrt1.Name = "cmbRangoCrt1";
            this.cmbRangoCrt1.Size = new System.Drawing.Size(217, 26);
            this.cmbRangoCrt1.TabIndex = 1;
            this.cmbRangoCrt1.SelectedIndexChanged += new System.EventHandler(this.cmbRangoCrt1_SelectedIndexChanged);
            // 
            // FrmMaestra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1177, 717);
            this.Controls.Add(this.tlp0All);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmMaestra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Panel de control";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tlp0All.ResumeLayout(false);
            this.tlpI1Informacion.ResumeLayout(false);
            this.tlp2InfoUsuario.ResumeLayout(false);
            this.tlp2InfoUsuario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLogo)).EndInit();
            this.tclPrincipal.ResumeLayout(false);
            this.tpgUsuario.ResumeLayout(false);
            this.tlp2panelUsuario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuario)).EndInit();
            this.tlp3CabeceraUsuario.ResumeLayout(false);
            this.tlp4Filtros.ResumeLayout(false);
            this.tlp4Filtros.PerformLayout();
            this.tlp4BotoneraUsuario.ResumeLayout(false);
            this.tlp4Gestiones.ResumeLayout(false);
            this.tlp4Gestiones.PerformLayout();
            this.tpg2Reporteria.ResumeLayout(false);
            this.tlp2Reporteria.ResumeLayout(false);
            this.tlp3Arbol.ResumeLayout(false);
            this.tlp4Titulo.ResumeLayout(false);
            this.tlp4Titulo.PerformLayout();
            this.tlp4Formulario.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInforme)).EndInit();
            this.tlp4Botonera.ResumeLayout(false);
            this.tlp3Graficos.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.crt2)).EndInit();
            this.tlp4Grafico1.ResumeLayout(false);
            this.tlp5Gr1Grafico.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.crt1)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp0All;
        private System.Windows.Forms.TableLayoutPanel tlpI1Informacion;
        private System.Windows.Forms.TableLayoutPanel tlp2InfoUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.TabControl tclPrincipal;
        private System.Windows.Forms.TabPage tpgUsuario;
        private System.Windows.Forms.TabPage tpg2Reporteria;
        private System.Windows.Forms.TableLayoutPanel tlp2panelUsuario;
        private System.Windows.Forms.DataGridView dgvUsuario;
        private System.Windows.Forms.TableLayoutPanel tlp3CabeceraUsuario;
        private System.Windows.Forms.Button btnUsu_Nuevo;
        private System.Windows.Forms.TableLayoutPanel tlp4Filtros;
        private System.Windows.Forms.ComboBox cmbUsu_Cargo;
        private System.Windows.Forms.ComboBox cmbUsu_perfil;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsu_Correo;
        private System.Windows.Forms.ComboBox cmbUsu_empresa;
        private System.Windows.Forms.TableLayoutPanel tlp4BotoneraUsuario;
        private System.Windows.Forms.Button btnUsu_Eliminar;
        private System.Windows.Forms.Button btnUsu_Editar;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TableLayoutPanel tlp4Gestiones;
        private System.Windows.Forms.Button btnCargo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEmpresa;
        private System.Windows.Forms.TableLayoutPanel tlp2Reporteria;
        private System.Windows.Forms.TableLayoutPanel tlp3Arbol;
        private System.Windows.Forms.TableLayoutPanel tlp4Titulo;
        private System.Windows.Forms.Label lblInfTitulo;
        private System.Windows.Forms.TableLayoutPanel tlp4Formulario;
        private System.Windows.Forms.DataGridView dgvInforme;
        private System.Windows.Forms.TableLayoutPanel tlp4Botonera;
        private System.Windows.Forms.Button btnVerProceso;
        private System.Windows.Forms.Button btnVolver;
        private System.Windows.Forms.TableLayoutPanel tlp3Graficos;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tlp4Grafico1;
        private System.Windows.Forms.TableLayoutPanel tlp5Gr1Grafico;
        private System.Windows.Forms.TableLayoutPanel tlp5Gr1Info;
        private System.Windows.Forms.DataVisualization.Charting.Chart crt1;
        private System.Windows.Forms.PictureBox pbxLogo;
        private System.Windows.Forms.DataVisualization.Charting.Chart crt2;
        private System.Windows.Forms.Button btnRepoRefrescar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbRangoCrt1;
    }
}

