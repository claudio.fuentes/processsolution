﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;

namespace PanelDeControl.frmEditar
{
    public partial class FrmEditEmpresa : Form
    {
        #region PROPIEDADES
        Empresa empresa;
        #endregion


        #region INICIADORES E INSTANCIADORES
        public FrmEditEmpresa(Empresa _empresa)
        {
            this.empresa = _empresa;
            InitializeComponent();
            CargarFormulario();

        }

        public void CargarFormulario()
        {
            txtEmpresa.Text = empresa.RazonSocial;
        }
        #endregion


        #region MÉTODOS

        #endregion


        #region ACCIONES DE CONTROL
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                empresa.RazonSocial = txtEmpresa.Text;
                if (Empresa.EditarEmpresa(empresa))
                {
                    MessageBox.Show($"La empresa ha sido actualizada exitosamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show($"No fue posible actualizar la empresa", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido el siguiente problema en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGuardar_Click(new object(), new EventArgs());
            }
        }

        #endregion

    }
}
