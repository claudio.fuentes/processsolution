﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;

namespace PanelDeControl.frmEditar
{
    public partial class FrmEditCargo : Form
    {
        #region PROPIEDADES
        Cargo cargo;
        #endregion

        #region INICIADORES E INSTANCIADORES
        public FrmEditCargo(Cargo _cargo)
        {
            this.cargo = _cargo;
            InitializeComponent();
            CargarFormulario();
        }
        public void CargarFormulario()
        {
            txtCargo.Text = cargo.Descripcion;
        }
        #endregion


        #region ACCIONES DE CONTROL
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                cargo.Descripcion = txtCargo.Text;
                if (Cargo.EditarCargo(cargo))
                {
                    MessageBox.Show($"El cargo ha sido actualizada exitosamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Close();
                }
                else
                {
                    MessageBox.Show($"No fue posible actualizar el cargo", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido el siguiente problema en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGuardar_Click(new object(), new EventArgs());
            }
        }

        #endregion
    }
}
