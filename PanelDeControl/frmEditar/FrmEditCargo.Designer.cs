﻿namespace PanelDeControl.frmEditar
{
    partial class FrmEditCargo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp0All = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Cabecera = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tlp1Formulario = new System.Windows.Forms.TableLayoutPanel();
            this.txtCargo = new System.Windows.Forms.TextBox();
            this.tlp1Botonera = new System.Windows.Forms.TableLayoutPanel();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.tlp0All.SuspendLayout();
            this.tlp1Cabecera.SuspendLayout();
            this.tlp1Formulario.SuspendLayout();
            this.tlp1Botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp0All
            // 
            this.tlp0All.ColumnCount = 1;
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlp0All.Controls.Add(this.tlp1Cabecera, 0, 0);
            this.tlp0All.Controls.Add(this.tlp1Formulario, 0, 1);
            this.tlp0All.Controls.Add(this.tlp1Botonera, 0, 2);
            this.tlp0All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp0All.Location = new System.Drawing.Point(0, 0);
            this.tlp0All.Margin = new System.Windows.Forms.Padding(4);
            this.tlp0All.Name = "tlp0All";
            this.tlp0All.RowCount = 3;
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlp0All.Size = new System.Drawing.Size(460, 236);
            this.tlp0All.TabIndex = 0;
            // 
            // tlp1Cabecera
            // 
            this.tlp1Cabecera.ColumnCount = 1;
            this.tlp1Cabecera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Cabecera.Controls.Add(this.label1, 0, 0);
            this.tlp1Cabecera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Cabecera.Location = new System.Drawing.Point(4, 4);
            this.tlp1Cabecera.Margin = new System.Windows.Forms.Padding(4);
            this.tlp1Cabecera.Name = "tlp1Cabecera";
            this.tlp1Cabecera.RowCount = 1;
            this.tlp1Cabecera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Cabecera.Size = new System.Drawing.Size(452, 39);
            this.tlp1Cabecera.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(446, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "editar cargo";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tlp1Formulario
            // 
            this.tlp1Formulario.ColumnCount = 1;
            this.tlp1Formulario.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Formulario.Controls.Add(this.txtCargo, 0, 0);
            this.tlp1Formulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Formulario.Location = new System.Drawing.Point(4, 51);
            this.tlp1Formulario.Margin = new System.Windows.Forms.Padding(4);
            this.tlp1Formulario.Name = "tlp1Formulario";
            this.tlp1Formulario.RowCount = 1;
            this.tlp1Formulario.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Formulario.Size = new System.Drawing.Size(452, 110);
            this.tlp1Formulario.TabIndex = 1;
            // 
            // txtCargo
            // 
            this.txtCargo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCargo.Location = new System.Drawing.Point(20, 41);
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.Size = new System.Drawing.Size(412, 27);
            this.txtCargo.TabIndex = 0;
            // 
            // tlp1Botonera
            // 
            this.tlp1Botonera.ColumnCount = 2;
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.Controls.Add(this.btnGuardar, 1, 0);
            this.tlp1Botonera.Controls.Add(this.btnCancelar, 0, 0);
            this.tlp1Botonera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Botonera.Location = new System.Drawing.Point(4, 169);
            this.tlp1Botonera.Margin = new System.Windows.Forms.Padding(4);
            this.tlp1Botonera.Name = "tlp1Botonera";
            this.tlp1Botonera.RowCount = 1;
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Botonera.Size = new System.Drawing.Size(452, 63);
            this.tlp1Botonera.TabIndex = 2;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGuardar.Location = new System.Drawing.Point(262, 12);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(153, 38);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancelar.Location = new System.Drawing.Point(36, 12);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(153, 38);
            this.btnCancelar.TabIndex = 0;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FrmEditCargo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 236);
            this.Controls.Add(this.tlp0All);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmEditCargo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmEditEmpresa";
            this.tlp0All.ResumeLayout(false);
            this.tlp1Cabecera.ResumeLayout(false);
            this.tlp1Cabecera.PerformLayout();
            this.tlp1Formulario.ResumeLayout(false);
            this.tlp1Formulario.PerformLayout();
            this.tlp1Botonera.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp0All;
        private System.Windows.Forms.TableLayoutPanel tlp1Cabecera;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tlp1Formulario;
        private System.Windows.Forms.TextBox txtCargo;
        private System.Windows.Forms.TableLayoutPanel tlp1Botonera;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnCancelar;
    }
}