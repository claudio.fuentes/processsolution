﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;


namespace PanelDeControl.frmEliminar
{
    public partial class FrmUsuarioEliminar : Form
    {
        Usuario usuario;
        public FrmUsuarioEliminar(Usuario _usuario)
        {
            this.usuario = _usuario;
            InitializeComponent();
            CargaEtiquetas();
        }

        #region FUNCIONES DE INICIO
        void CargaEtiquetas()
        {
            lblUsuario.Text = usuario.NombreUsuario;
            lblNombre.Text = usuario.Nombre;
            lblApellido.Text = usuario.Apellido;
            lblCorreo.Text = usuario.Correo;
            lblEmpresa.Text = Empresa.EmpresaByID(usuario.EmpresaID).RazonSocial;
            lblPerfil.Text = Perfil.perfilByID(usuario.PerfilID).Descripcion;
            lblCargo.Text = Cargo.CargoByID(usuario.CargoID).Descripcion;
        }
        #endregion

        #region FUNCIONES DE ELIMINACIÓN
        void ValidarRelacionesTarea()
        {
            try
            {
                string mensaje = "";
                List<Tarea> listaTareas = usuario.listaTareasAsociadas();
                if (listaTareas != null && listaTareas.Count > 0)
                {

                    mensaje = $"No es posible eliminar este usuario ya que presenta {listaTareas.Count} relaciones con tarea";
                    MessageBox.Show(mensaje,"Aviso",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                }
                else
                {
                    ConfirmarRelacionHistoricos();
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void ConfirmarRelacionHistoricos()
        {
            try
            {
                int totalHT = usuario.listaHistoricoTareasAsociadas().Count;
                if (totalHT != 0)
                {
                    string mensaje = $"existen {totalHT} registros historicos.\nSi elimina el usuarios estos también se borrarán\n¿Está seguro que desea continuar?";
                    DialogResult dialogResult = MessageBox.Show(mensaje, "¡Atención!", MessageBoxButtons.YesNo,MessageBoxIcon.Exclamation);
                    if (dialogResult == DialogResult.No)
                    {
                        return;
                    }
                }
                if (Usuario.EliminarUsuarioHT((int)usuario.IdUsuario, out totalHT))
                {
                    MessageBox.Show($"Se ha eliminado el usuario {usuario.Nombre}\nSe han eliminado {totalHT} registros historicos.", "Usuario eliminado exitosamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show($"Ha ocurrido un error en la ejecución", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region ACCIONES DE CONTROL
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        #endregion

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            ValidarRelacionesTarea();
            Close();
        }
    }
}
