﻿namespace PanelDeControl.frmEliminar
{
    partial class FrmUsuarioEliminar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarioEliminar));
            this.tlp0All = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Cabecera = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Info = new System.Windows.Forms.TableLayoutPanel();
            this.tlp1Botonera = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblPerfil = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblCargo = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblEmpresa = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.tlp0All.SuspendLayout();
            this.tlp1Cabecera.SuspendLayout();
            this.tlp1Info.SuspendLayout();
            this.tlp1Botonera.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlp0All
            // 
            this.tlp0All.ColumnCount = 1;
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp0All.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tlp0All.Controls.Add(this.tlp1Cabecera, 0, 0);
            this.tlp0All.Controls.Add(this.tlp1Info, 0, 1);
            this.tlp0All.Controls.Add(this.tlp1Botonera, 0, 2);
            this.tlp0All.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp0All.Location = new System.Drawing.Point(0, 0);
            this.tlp0All.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.tlp0All.Name = "tlp0All";
            this.tlp0All.RowCount = 3;
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tlp0All.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp0All.Size = new System.Drawing.Size(549, 508);
            this.tlp0All.TabIndex = 0;
            // 
            // tlp1Cabecera
            // 
            this.tlp1Cabecera.ColumnCount = 1;
            this.tlp1Cabecera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Cabecera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp1Cabecera.Controls.Add(this.label1, 0, 0);
            this.tlp1Cabecera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Cabecera.Location = new System.Drawing.Point(3, 3);
            this.tlp1Cabecera.Name = "tlp1Cabecera";
            this.tlp1Cabecera.RowCount = 1;
            this.tlp1Cabecera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Cabecera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp1Cabecera.Size = new System.Drawing.Size(543, 95);
            this.tlp1Cabecera.TabIndex = 0;
            // 
            // tlp1Info
            // 
            this.tlp1Info.ColumnCount = 2;
            this.tlp1Info.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Info.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Info.Controls.Add(this.lblEmpresa, 1, 6);
            this.tlp1Info.Controls.Add(this.label14, 0, 6);
            this.tlp1Info.Controls.Add(this.lblCargo, 1, 5);
            this.tlp1Info.Controls.Add(this.label12, 0, 5);
            this.tlp1Info.Controls.Add(this.lblPerfil, 1, 4);
            this.tlp1Info.Controls.Add(this.label10, 0, 4);
            this.tlp1Info.Controls.Add(this.lblCorreo, 1, 3);
            this.tlp1Info.Controls.Add(this.label8, 0, 3);
            this.tlp1Info.Controls.Add(this.lblApellido, 1, 2);
            this.tlp1Info.Controls.Add(this.label6, 0, 2);
            this.tlp1Info.Controls.Add(this.lblNombre, 1, 1);
            this.tlp1Info.Controls.Add(this.label4, 0, 1);
            this.tlp1Info.Controls.Add(this.lblUsuario, 1, 0);
            this.tlp1Info.Controls.Add(this.label2, 0, 0);
            this.tlp1Info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Info.Location = new System.Drawing.Point(3, 104);
            this.tlp1Info.Name = "tlp1Info";
            this.tlp1Info.RowCount = 7;
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tlp1Info.Size = new System.Drawing.Size(543, 349);
            this.tlp1Info.TabIndex = 1;
            // 
            // tlp1Botonera
            // 
            this.tlp1Botonera.ColumnCount = 2;
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp1Botonera.Controls.Add(this.btnEliminar, 1, 0);
            this.tlp1Botonera.Controls.Add(this.btnCancelar, 0, 0);
            this.tlp1Botonera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp1Botonera.Location = new System.Drawing.Point(3, 459);
            this.tlp1Botonera.Name = "tlp1Botonera";
            this.tlp1Botonera.RowCount = 1;
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp1Botonera.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp1Botonera.Size = new System.Drawing.Size(543, 46);
            this.tlp1Botonera.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(537, 95);
            this.label1.TabIndex = 0;
            this.label1.Text = "Se eliminará el siguiente usuario";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 49);
            this.label2.TabIndex = 0;
            this.label2.Text = "usuario";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUsuario.Location = new System.Drawing.Point(274, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(266, 49);
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "usuario";
            this.lblUsuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(265, 49);
            this.label4.TabIndex = 2;
            this.label4.Text = "nombre";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNombre.Location = new System.Drawing.Point(274, 49);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(266, 49);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "usuario";
            this.lblNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(265, 49);
            this.label6.TabIndex = 4;
            this.label6.Text = "apellido";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblApellido.Location = new System.Drawing.Point(274, 98);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(266, 49);
            this.lblApellido.TabIndex = 5;
            this.lblApellido.Text = "usuario";
            this.lblApellido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(3, 147);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(265, 49);
            this.label8.TabIndex = 6;
            this.label8.Text = "correo";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCorreo.Location = new System.Drawing.Point(274, 147);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(266, 49);
            this.lblCorreo.TabIndex = 7;
            this.lblCorreo.Text = "usuario";
            this.lblCorreo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(3, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(265, 49);
            this.label10.TabIndex = 8;
            this.label10.Text = "perfil";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPerfil
            // 
            this.lblPerfil.AutoSize = true;
            this.lblPerfil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPerfil.Location = new System.Drawing.Point(274, 196);
            this.lblPerfil.Name = "lblPerfil";
            this.lblPerfil.Size = new System.Drawing.Size(266, 49);
            this.lblPerfil.TabIndex = 9;
            this.lblPerfil.Text = "usuario";
            this.lblPerfil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(3, 245);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(265, 49);
            this.label12.TabIndex = 10;
            this.label12.Text = "cargo";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCargo
            // 
            this.lblCargo.AutoSize = true;
            this.lblCargo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCargo.Location = new System.Drawing.Point(274, 245);
            this.lblCargo.Name = "lblCargo";
            this.lblCargo.Size = new System.Drawing.Size(266, 49);
            this.lblCargo.TabIndex = 11;
            this.lblCargo.Text = "usuario";
            this.lblCargo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(3, 294);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(265, 55);
            this.label14.TabIndex = 12;
            this.label14.Text = "empresa";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEmpresa
            // 
            this.lblEmpresa.AutoSize = true;
            this.lblEmpresa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEmpresa.Location = new System.Drawing.Point(274, 294);
            this.lblEmpresa.Name = "lblEmpresa";
            this.lblEmpresa.Size = new System.Drawing.Size(266, 55);
            this.lblEmpresa.TabIndex = 13;
            this.lblEmpresa.Text = "usuario";
            this.lblEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCancelar.Location = new System.Drawing.Point(55, 7);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(160, 31);
            this.btnCancelar.TabIndex = 0;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminar.Location = new System.Drawing.Point(327, 7);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(160, 31);
            this.btnEliminar.TabIndex = 1;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // FrmUsuarioEliminar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(549, 508);
            this.Controls.Add(this.tlp0All);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "FrmUsuarioEliminar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmUsuarioEliminar";
            this.tlp0All.ResumeLayout(false);
            this.tlp1Cabecera.ResumeLayout(false);
            this.tlp1Cabecera.PerformLayout();
            this.tlp1Info.ResumeLayout(false);
            this.tlp1Info.PerformLayout();
            this.tlp1Botonera.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp0All;
        private System.Windows.Forms.TableLayoutPanel tlp1Cabecera;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tlp1Info;
        private System.Windows.Forms.Label lblEmpresa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblCargo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblPerfil;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tlp1Botonera;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCancelar;
    }
}