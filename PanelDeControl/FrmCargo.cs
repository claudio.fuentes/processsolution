﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProcessApi.Models;
using PanelDeControl.frmNuevos;
using PanelDeControl.frmEditar;

namespace PanelDeControl
{
    public partial class FrmCargo : Form
    {
        #region PROPIEDADES
        DataTable dttCargo = new DataTable();
        #endregion


        #region INSTANCIADORES E INICIALIZADORES
        public FrmCargo()
        {
            InitializeComponent();
            InstanciadorDGV();
        }
        public void InstanciadorDGV()
        {
            try
            {
                dttCargo.Columns.Add("idCargo");
                dttCargo.Columns.Add("Cargo");

                dttCargo.Columns[1].ColumnName = "descripcion";

                dttCargo.Columns[0].ColumnMapping = MappingType.Hidden;

                CargaDGV();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void CargaDGV()
        {
            try
            {
                dttCargo.Rows.Clear();
                foreach (Cargo item in Cargo.listaCargos())
                {
                    object[] cargo = new object[]
                    {
                        item.IdCargo,
                        item.Descripcion
                    };
                    dttCargo.Rows.Add(cargo);
                }

                dgvCargo.DataSource = dttCargo;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución.\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region ACCIÓN DE CONTROLES
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmAddCargo frmAddCargo = new FrmAddCargo();
            frmAddCargo.ShowDialog();
            CargaDGV();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                var tablaFila = dgvCargo.SelectedRows[0];
                int dataFilaId = int.Parse(dttCargo.Rows[tablaFila.Index][0].ToString());
                string dataFilaName = dttCargo.Rows[tablaFila.Index][1].ToString();

                DialogResult dialogResult = MessageBox.Show($"¿está seguro que desea eliminar el cargo {dataFilaName}?", "Advertencia", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }

                int usuariosAsociados = 0;
                

                Cargo.UsuariosASociados(dataFilaId);
                if (usuariosAsociados > 0)
                {
                    MessageBox.Show($"No es posible eliminar el cargo ya que esta posee las siguientes asociaciones con otras tablas:\n{usuariosAsociados} usuarios\nprocure que el cargo no tenga estas asociaciones y luego intente eliminarla nuevamente", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }

                if (Cargo.EliminarCArgo(dataFilaId))
                {
                    MessageBox.Show($"el cargo \"{dataFilaName}\" ha sido eliminada exitosamente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargaDGV();
                }
                else
                {
                    MessageBox.Show($"No ha sido posible eliminar el cargo seleccionado", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                var tablaFila = dgvCargo.SelectedRows[0];
                int dataFilaId = int.Parse(dttCargo.Rows[tablaFila.Index][0].ToString());
                string dataFilaName = dttCargo.Rows[tablaFila.Index][1].ToString();

                Cargo cargoSeleccionado = new Cargo(dataFilaId, dataFilaName);
                FrmEditCargo frmEditCargo = new FrmEditCargo(cargoSeleccionado);
                frmEditCargo.Owner = this;
                frmEditCargo.ShowDialog(this);
                CargaDGV();

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ha ocurrido un error en la ejecución\nError:\n{ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        #endregion
    }
}
